# Generest - REST API Generator

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.sdugene%3Agenerest-maven-plugin&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.sdugene%3Agenerest-maven-plugin)

_Spring Boot rest API generator maven plugin._  
_Generate the CRUD endpoints with entities, repositories, services, dtos, mappers and controllers ; fully customizable and easy to maintain._

Run ``mvn generest:run`` on project root.  

_This will delete all files contained in generest.package defined in generest.xml then create all new files updated from database structure._

**Today, works only with PostgreSQL and MySQL database.**

### * Needed configuration

Create a generest.xml file in project root containing :

```
<configuration>
    <datasource>
        <url>jdbc:postgresql://database_url:database_port/base_name</url>   <!-- PostgreSQL Db -->
        <url>jdbc:mysql://database_url:database_port</url>                  <!-- MySQL Db -->
        <username>username</username>
        <password>password</password>
        <schema>schema</schema>
    </datasource>

    <java>
        <folder>src/main/java/com/my/project</folder>
        <package>com.my.project</package>
    </java>

    <generest>
        <package>generest</package>                 <!-- optionnal -->
        <suffix>Gen</suffix>                        <!-- optionnal -->
        <apiUri>/api/v2</apiUri>                    <!-- optionnal -->
    </generest>

    <generated>
        <controllers>true</controllers>             <!-- optionnal -->
        <dtos>true</dtos>                           <!-- optionnal -->
        <filters>true</filters>                     <!-- optionnal -->
        <services>true</services>                   <!-- optionnal -->
        <mappers>true</mappers>                     <!-- optionnal -->
    </generated>
    
    <exclusions>
        <entities>                                  <!-- optionnal disabling classes importing it -->
            <my_table/>                             <!-- optionnal -->
        </entities>                                 <!-- optionnal -->
        <repositories>                              <!-- optionnal disabling classes importing it -->
            <my_table/>                             <!-- optionnal -->
        </repositories>                             <!-- optionnal -->
        <controllers>                               <!-- optionnal -->
            <my_table/>                             <!-- optionnal -->
        </controllers>                              <!-- optionnal -->
        <dtos>                                      <!-- optionnal disabling classes importing it -->
            <my_table/>                             <!-- optionnal -->
        </dtos>                                     <!-- optionnal -->
        <filters>                                   <!-- optionnal -->
            <my_table/>                             <!-- optionnal -->
        </filters>                                  <!-- optionnal -->
        <services>                                  <!-- optionnal disabling classes importing it -->
            <my_table/>                             <!-- optionnal -->
        </services>                                 <!-- optionnal -->
        <mappers>                                   <!-- optionnal disabling classes importing it -->
            <my_table/>                             <!-- optionnal -->
        </mappers>                                  <!-- optionnal -->

    <parameters>
    <lombok>false</lombok>                          <!-- optionnal -->
    <dtoChangeLog>true</dtoChangeLog>               <!-- optionnal -->    
    <lazyLoading>true</lazyLoading>                 <!-- optionnal -->
    <jpaSpecification>true</jpaSpecification>       <!-- optionnal disabling filters -->
    <timestampType></timestampType>                 <!-- optionnal default LOCAL_DATE_TIME values LOCAL_TIME | LOCAL_DATE | LOCAL_DATE_TIME | TIMESTAMP -->
    </parameters>

    <documentations>
        <swagger>true</swagger>                     <!-- optionnal -->
        <api>true</api>                             <!-- optionnal -->
    </documentations>
</configuration>
```

### * pom.xml adds

##### repositories :
```
<pluginRepositories>
    ...
    <pluginRepository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </pluginRepository>
</pluginRepositories>
```

##### build plugins :
```
<plugins>
    ...
    <plugin>
        <groupId>com.gitlab.sdugene</groupId>
        <artifactId>generest</artifactId>
        <version>last_version</version>
    </plugin>
</plugin>
```

### * Spring configuration (src/main/resources/application.properties)      -- PostgreSQL Db --
```
spring.datasource.url=jdbc:postgresql://database_url:database_port/base_name
spring.datasource.username=username
spring.datasource.password=password
spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.PostgreSQLDialect
spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true
```

### * Spring configuration (src/main/resources/application.properties)      -- MySQL Db --
```
spring.datasource.url=jdbc:mysql://database_url:database_port/base_name
spring.datasource.username=username
spring.datasource.password=password
spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5Dialect
```

### * Main Application.java (src/main/java/com/my/project/Application.java)
```
package com.my.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
```

### *  Needed dependencies

##### Latest dependency versions
* **SPRING_VERSION** : 2.1.2.RELEASE
 

```
<!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-data-jpa -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
    <version>SPRING_VERSION</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-web -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    <version>SPRING_VERSION</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-test -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <version>SPRING_VERSION</version>
    <scope>test</scope>
</dependency>
```

##### Latest tested database dependency versions
* **POSTGRES_VERSION** : 42.2.5  
* **MYSQL_VERSION** : 8.0.15

```
<!-- https://mvnrepository.com/artifact/org.postgresql/postgresql -->
<dependency>
    <groupId>org.postgresql</groupId>
    <artifactId>postgresql</artifactId>
    <version>POSTGRES_VERSION</version>
</dependency>

<!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>MYSQL_VERSION</version>
</dependency>
```

##### If swagger option enabled
* **SWAGGER_VERSION** : 2.9.2  

```
<!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger-ui -->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>SWAGGER_VERSION</version>
</dependency>

<!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger2 -->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>SWAGGER_VERSION</version>
</dependency>
```

##### If lombok option enabled
* **LOMBOK_VERSION** : 1.18.10 

```
<!-- https://mvnrepository.com/artifact/org.projectlombok/lombok -->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>LOMBOK_VERSION</version>
    <scope>provided</scope>
</dependency>
```