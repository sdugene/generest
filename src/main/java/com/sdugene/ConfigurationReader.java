package com.sdugene;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.sdugene.enums.DbType;
import com.sdugene.models.Configuration;

import java.io.*;
import java.util.*;

import static com.sdugene.utils.StringUtils.inputStreamToString;

class ConfigurationReader {

    List<String> errors = new ArrayList<>();

    private Configuration configuration;

    boolean read(String configurationFile) throws IOException {

        File file = new File(configurationFile);

        XmlMapper xmlMapper = new XmlMapper();
        configuration = xmlMapper.readValue(inputStreamToString(new FileInputStream(file)), Configuration.class);

        String databaseUrl = configuration.getDatasource().getUrl();
        if (databaseUrl == null) errors.add("database url property not found");

        String databaseUsername = configuration.getDatasource().getUsername();
        if (databaseUsername == null) errors.add("database username property not found");

        String databasePassword = configuration.getDatasource().getPassword();
        if (databasePassword == null) errors.add("database password property not found");

        DbType databaseType = configuration.getDatasource().getType();
        if (databaseType == null) errors.add("database type property not found");

        String databaseSchema = configuration.getDatasource().getSchema();
        if (databaseSchema == null) errors.add("database schema property not found");

        String javaFolder = configuration.getJava().getFolder();
        if (javaFolder == null) errors.add("java folder property not found");

        String packageName = configuration.getJava().getDefaultPackage();
        if (packageName == null) errors.add("package name property not found");

        return (databaseUrl != null
                && databaseUsername != null
                && databasePassword != null
                && databaseType != null
                && databaseSchema != null
                && javaFolder != null
                && packageName != null);
    }

    Configuration getConfiguration() { return configuration; }
}
