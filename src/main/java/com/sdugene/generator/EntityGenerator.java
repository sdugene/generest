package com.sdugene.generator;

import com.sdugene.Constant;
import com.sdugene.Function;
import com.sdugene.enums.Accessor;
import com.sdugene.enums.DataClassType;
import com.sdugene.enums.DbType;
import com.sdugene.enums.Type;
import com.sdugene.models.Annotation;
import com.sdugene.models.Column;
import com.sdugene.models.Configuration;
import com.sdugene.models.Table;
import com.sdugene.models.annotation.Entity;

import java.util.ArrayList;
import java.util.List;

public class EntityGenerator extends Generator {

    private EntityGenerator() {
        throw new IllegalStateException("Utility class");
    }

    public static List<String> generateClass(Table table, Configuration configuration, Annotation annotation) {
        setVariables(table, configuration);
        Entity classAnnotations = annotation.getEntities(table.getName());
        addHeader();

        content.add("");
        content.add("package " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.ENTITIES + ";");

        content.add("");
        generateEntityImport();
        if (classAnnotations != null && !classAnnotations.getClassImport().isEmpty()) {
            content.addAll(classAnnotations.getClassImport());
        }
        if (configuration.getParameters().isLombok()) {
            generateLombokImport(DataClassType.ENTITY);
        }

        content.add("");

        if (table.isView()) {
            content.add("// Entity generated from database view");
            content.add("@Entity");
            content.add("@Immutable");
        } else {
            content.add("@Entity");
        }
        if (classAnnotations != null && !classAnnotations.getClassAnnotations().isEmpty()) {
            content.addAll(classAnnotations.getClassAnnotations());
        }
        if (configuration.getParameters().isLombok()) {
            generateLombokAnnotation(DataClassType.ENTITY);
        }
        generateEntityTypeDef();


        if (DbType.POSTGRESQL.equals(configuration.getDatasource().getType())) {
            content.add("@Table(name = \"" + table.getSqlName() + "\", schema = \"" + configuration.getDatasource().getSchema() + "\")");
        } else {
            content.add("@Table(name = \"" + table.getSqlName() + "\")");
        }

        content.add(Accessor.PUBLIC.getAccess() + " class " + entityName + " {");

        // Attributes
        List<String> attributes = new ArrayList<>();
        EntityAttributeGenerator.generateEntityAttribute(attributes, classAnnotations);

        // Constructors
        List<String> constructors = new ArrayList<>();
        if (!configuration.getParameters().isLombok()) {
            generateClassEmptyConstructorMethod(constructors, DataClassType.ENTITY);
            generateClassAllArgsConstructorMethod(constructors, DataClassType.ENTITY);
            if (!table.isView()) {
                generateClassAllArgsConstructorMethod(constructors, DataClassType.ENTITY_BUILDER);
            }
        }
        generateEntityConstructorCopyMethod(constructors);

        // Setters & Getters
        List<String> setters = new ArrayList<>();
        List<String> getters = new ArrayList<>();
        List<String> methods = new ArrayList<>();

        if (!configuration.getParameters().isLombok()) {
            generateSetter(setters, DataClassType.ENTITY);
            generateGetter(getters, DataClassType.ENTITY);
            generateToStringMethod(DataClassType.ENTITY, methods, false);
        }

        generateClassContent(attributes, constructors, setters, getters, methods);

        if (!configuration.getParameters().isLombok() && !table.isView()) {
            generateBuilder(DataClassType.ENTITY_BUILDER);
        }

        generateEquals(DataClassType.ENTITY);

        content.add("}");
        return content;
    }

    public static List<String> generateFilterClass(Table table, Configuration configuration) {
        setVariables(table, configuration);

        addHeader();

        content.add(Constant.EMPTY_LINE);
        content.add("package " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.ENTITIES + "." + Constant.FILTERS + ";");

        content.add(Constant.EMPTY_LINE);
        generateFilterImport();
        content.add("");

        content.add(Accessor.PUBLIC.getAccess() + " class " + filterName + " {");

        // Attributes
        List<String> attributes = new ArrayList<>();
        EntityAttributeGenerator.generateFilterAttribute(attributes);

        // Constructors
        List<String> constructors = new ArrayList<>();
        generateFilterEmptyConstructorMethod(constructors);
        generateClassAllArgsConstructorMethod(constructors, DataClassType.FILTER);

        // Setters
        List<String> setters = new ArrayList<>();
        generateSetter(setters, DataClassType.FILTER);

        // Getters
        List<String> getters = new ArrayList<>();
        generateGetter(getters, DataClassType.FILTER);

        generateClassContent(attributes, constructors, setters, getters);

        generateBuilder(DataClassType.FILTER);

        content.add("}");
        return content;
    }



    private static void generateFilterEmptyConstructorMethod(List<String> constructors) {
        constructors.add(Constant.EMPTY_LINE);
        constructors.add("\t" + Accessor.PUBLIC.getAccess() + " " + filterName + "() {}");
    }

    private static void generateEntityConstructorCopyMethod(List<String> constructors) {
        constructors.add("");
        String entitySourceAttribute = Function.snakeToLowCamel(table.getName()) + "Source";

        constructors.add("\t" + Accessor.PUBLIC.getAccess() + " " + entityName + "(" + entityName + " " + entitySourceAttribute + ") {");

        for (Column column : table.getColumnList()) {
            String columnAttribute = Function.snakeToLowCamel(column.getName());
            if (column.getConstraintName() != null) {
                columnAttribute = Function.snakeToLowCamel(column.getFkAttribute());
            }

            constructors.add("\t\tthis." + columnAttribute + " = " + entitySourceAttribute + ".get" + Function.snakeToCamel(columnAttribute) + "();");
        }
        constructors.add("\t}");
    }

    private static void generateEntityTypeDef() {
        List<String> typePresent = new ArrayList<>();

        for (Column column : table.getColumnList()) {
            if (Type.ENUM.equals(column.getType()) && !typePresent.contains(Type.ENUM.getTypeName())) {
                content.add("@TypeDef(name = \"pgSql_enum\", typeClass = PostgreSQLEnumType.class)");
                typePresent.add(Type.ENUM.getTypeName());
            }
            if (Type.JSONB.equals(column.getType()) && !typePresent.contains(Type.JSONB.getTypeName())) {
                content.add("@TypeDef(name = \"jsonb\", typeClass = JsonBinaryType.class)");
                typePresent.add(Type.JSONB.getTypeName());
            }
        }
    }

    private static void generateEntityImport() {
        content.add("import javax.persistence.Column;");
        content.add("import javax.persistence.Entity;");
        content.add("import java.util.Objects;");

        if (table.getColumnList().stream().anyMatch(column -> column.isPrimary(table.getColumnList()))
                || table.isView()) {
            content.add("import javax.persistence.Id;");
        }
        if (table.isView()) {
            content.add("import org.hibernate.annotations.Immutable;");
        }
        content.add("import javax.persistence.Table;");
        List<String> importPresent = new ArrayList<>();

        if (!Type.UUID.equals(table.getColumnList().get(0).getType())) {
            content.add("import javax.persistence.GeneratedValue;");
            content.add("import javax.persistence.GenerationType;");
            importPresent.add("generatedId");
        }

        for (Column column : table.getColumnList()) {
            generateTypeImport(column, importPresent, true);
            generateEnumImport(column, importPresent);

            if (dates.contains(column.getType()) && !importPresent.contains("temporal")) {
                content.add("import javax.persistence.Temporal;");
                content.add("import javax.persistence.TemporalType;");
                importPresent.add("temporal");
            }

            if (Type.ENUM.equals(column.getType()) || Type.JSONB.equals(column.getType())) {
                generateEntityTypeDefImport(column, importPresent);
            }

            if (column.getConstraintName() != null && !importPresent.contains("Join")) {
                content.add("import javax.persistence.JoinColumn;");
                content.add("import javax.persistence.ManyToOne;");
                content.add("import javax.persistence.FetchType;");
                content.add("import javax.persistence.ForeignKey;");
                importPresent.add("Join");
            }
        }
    }

    private static void generateFilterImport() {
        List<String> importPresent = new ArrayList<>();

        for (Column column : table.getColumnList()) {
            generateTypeImport(column, importPresent, false);
            generateEnumImport(column, importPresent);
        }
    }

    private static void generateEntityTypeDefImport(Column column, List<String> importPresent) {
        if (Type.ENUM.equals(column.getType()) && !importPresent.contains(Type.ENUM.getTypeName())) {
            content.add("import javax.persistence.EnumType;");
            content.add("import javax.persistence.Enumerated;");
            content.add("import " + configuration.getJava().getDefaultPackageDotted() +
                    configuration.getGenerest().getPackageNameDoted() + Constant.ENUMS + ".PostgreSQLEnumType;");
            importPresent.add(Type.ENUM.getTypeName());
        }

        if (Type.JSONB.equals(column.getType()) && !importPresent.contains(Type.JSONB.getTypeName())) {
            content.add("import com.vladmihalcea.hibernate.type.json.JsonBinaryType;");
            importPresent.add(Type.JSONB.getTypeName());
        }

        if (!importPresent.contains("TypeDef")) {
            content.add("import org.hibernate.annotations.Type;");
            content.add("import org.hibernate.annotations.TypeDef;");
            importPresent.add("TypeDef");
        }
    }
}
