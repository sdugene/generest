package com.sdugene.generator;

import com.sdugene.Constant;
import com.sdugene.Function;
import com.sdugene.enums.Accessor;
import com.sdugene.enums.Type;
import com.sdugene.models.Column;
import com.sdugene.models.annotation.Entity;
import java.util.List;

class EntityAttributeGenerator extends Generator {

    private EntityAttributeGenerator() {
        throw new IllegalStateException("Utility class");
    }

    static void generateEntityAttribute(List<String> attributes, Entity classAnnotations) {
        for (Column column : table.getColumnList()) {
            final String columnName = Function.snakeToLowCamel(column.getName());
            String columnType = column.getType().getTypeName();
            attributes.add("");

            if (dates.contains(column.getType())) {
                columnType = getDateType(column.getType()).getTypeName();
            }

            if (classAnnotations != null && classAnnotations.getAttributeAnnotations().containsKey(column.getName())) {
                classAnnotations
                        .getAttributeAnnotations()
                        .get(column.getName())
                        .forEach(s -> attributes.add("\t" + s));
            }

            if (column.isPrimary(table.getColumnList()) || (table.isView() && 0 == column.getIndex())) {
                attributes.add("\t@Id");
                if (!Type.UUID.equals(column.getType()) && !table.isView()) {
                    attributes.add("\t@GeneratedValue(strategy = GenerationType.IDENTITY)");
                }
                attributes.add(Constant. TAB_AT_COLUMN + "(" + Constant.NAME + " = " + "\"" + column.getSqlName() + "\")");
                attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + columnType + " " + columnName + ";");
            } else if (column.getConstraintName() != null) {
                constraintAttribute(column, attributes);
            } else if (Type.ENUM.equals(column.getType())) {
                enumAttribute(column, attributes);
            } else if (Type.JSONB.equals(column.getType())) {
                jsonbAttribute(column, attributes);
            } else {
                attributes.add(Constant. TAB_AT_COLUMN + "(" + Constant.NAME + " = " + "\"" + column.getSqlName() + "\")");
                attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + columnType + " " + columnName + ";");
            }
        }
    }

    static void generateFilterAttribute(List<String> attributes) {
        attributes.add("");
        for (Column column : table.getColumnList()) {
            if (Type.ENUM.equals(column.getType())) {
                attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix() +" " + Function.snakeToLowCamel(column.getName()) + ";");
            } else if (Type.JSONB.equals(column.getType())) {
                attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + Type.STRING.getTypeName() +" " + Function.snakeToLowCamel(column.getName()) + ";");
            } else if (dates.contains(column.getType())) {
                attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + getDateType(column.getType()).getTypeName() + " " + Function.snakeToLowCamel(column.getName()) + ";");
            } else {
                attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + column.getType().getTypeName() + " " + Function.snakeToLowCamel(column.getName()) + ";");
            }
        }
    }

    private static void constraintAttribute(Column column, List<String> attributes) {
        if (column.getConstraintName() != null) {
            StringBuilder manyToOne = new StringBuilder();
            manyToOne.append("\t@ManyToOne(");
            if (column.isNullable()) {
                manyToOne.append("optional = true, ");
            }

            if (configuration.getParameters().isLazyLoading()) {
                manyToOne.append("fetch = FetchType.LAZY)");
            } else {
                manyToOne.append("fetch = FetchType.EAGER)");
            }
            attributes.add(manyToOne.toString());

            attributes.add("\t@JoinColumn(name = \"" + column.getSqlName() + "\", foreignKey = @ForeignKey(name = \"" + column.getConstraintName() + "\"))");
            attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + Function.snakeToCamel(column.getFkTable()) + Constant.ENTITY + configuration.getGenerest().getSuffix() + " " + Function.snakeToLowCamel(column.getFkAttribute()) + ";");
        }
    }

    private static void enumAttribute(Column column, List<String> attributes) {
        if (Type.ENUM.equals(column.getType())) {
            attributes.add(Constant. TAB_AT_COLUMN + "(" + Constant.NAME + " = " + "\"" + column.getName() + "\", columnDefinition = \" " + column.getEnumName() + "\")");
            attributes.add("\t@Enumerated(EnumType.STRING)");
            attributes.add("\t@Type(type = \"pgSql_enum\")");
            attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix() + " " + Function.snakeToLowCamel(column.getName()) + ";");
        }
    }

    private static void jsonbAttribute(Column column, List<String> attributes) {
        if (Type.JSONB.equals(column.getType())) {
            attributes.add("\t@Type(type = \"jsonb\")");
            attributes.add(Constant. TAB_AT_COLUMN + "(" + Constant.NAME + " = " + "\"" + column.getName() + "\", columnDefinition = \"jsonb\")");
            attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + Type.STRING.getTypeName() + " " + Function.snakeToLowCamel(column.getName()) + ";");
        }
    }
}
