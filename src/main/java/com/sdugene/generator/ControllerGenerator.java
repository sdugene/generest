package com.sdugene.generator;

import com.sdugene.Constant;
import com.sdugene.Function;
import com.sdugene.enums.Accessor;
import com.sdugene.enums.Type;
import com.sdugene.models.Column;
import com.sdugene.models.Configuration;
import com.sdugene.models.Table;

import java.util.ArrayList;
import java.util.List;

import static com.sdugene.Constant.*;

public class ControllerGenerator extends Generator {

    private ControllerGenerator() {
        throw new IllegalStateException("Utility class");
    }

    public static List<String> generateClass(Table table, Configuration configuration) {
        setVariables(table, configuration);
        addHeader();

        content.add("");
        content.add("package " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.CONTROLLERS + ";");

        content.add("");
        generateControllerImport();

        content.add("");
        content.add("@RestController");
        if (configuration.getDocumentations().isSwagger()) {
            content.add("@Api(tags = \"" + Function.snakeToCamel(table.getName()) + "\")");
        }
        content.add("@RequestMapping(\"" + configuration.getGenerest().getApiUri() + "/" + Function.snakeToLowCamel(table.getName()) + "\")");
        content.add(Accessor.PUBLIC.getAccess() + " class " + controllerName + " {");

        // Attributes
        List<String> attributes = new ArrayList<>();
        generateControllerAttribute(attributes);

        // Setters
        List<String> setters = new ArrayList<>();
        generateControllerSetter(setters);

        // Methods
        List<String> methods = new ArrayList<>();
        addControllerGetByIdMethod(methods);
        addControllerGetForeignByIdMethod(methods);
        addControllerGetEnum(methods);
        addControllerGetAllMethod(methods);
        if (configuration.getGenerated().isFilters()) {
            addControllerGetAllWithFilterMethod(methods);
        }

        if (!table.isView()) {
            addControllerCreateMethod(methods);
            addControllerUpdateMethod(methods);
            addControllerDeleteMethod(methods);
        }

        content.add("");
        generateClassContent(attributes, setters, methods);

        content.add("}");
        return content;
    }

    private static void addControllerGetByIdMethod(List<String> method) {
        String idName = entityAttribute + Function.snakeToCamel(table.getColumnList().get(0).getName());
        Type idType = table.getColumnList().get(0).getType();

        method.add("");
        method.add(TAB_X_1 + "@GetMapping(\"/{" + idName + "}\")");
        if (configuration.getDocumentations().isSwagger()) {
            method.add(TAB_X_1 + "@ApiOperation(value = \"Get " + Function.snakeToLowCamel(table.getName()) + " by " + Function.snakeToLowCamel(table.getColumnList().get(0).getName()) + "\", response = " + dtoFullName + ".class)");
            addApiResponse200(dtoFullName, "", method);
        }
        method.add(TAB_X_1 + Accessor.PUBLIC.getAccess() + " ResponseEntity<" + dtoFullName + "> get(@PathVariable(\"" + idName + "\") " + idType.getTypeName() + " " + idName + ") {");
        method.add(TAB_X_2 + "return " + serviceAttribute + ".get(" + idName + ")");
        method.add(TAB_X_2 + TAB_X_2 +".map(entity -> ResponseEntity.ok(" + mapperName + ".to" + dtoFullName + "(entity)))");
        method.add(TAB_X_2 + TAB_X_2 +".orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));");
        method.add(TAB_X_1 + "}");
    }

    private static void addControllerGetForeignByIdMethod(List<String> method) {
        if (!table.getJoinList().isEmpty()) {
            String idName = entityAttribute + Function.snakeToCamel(table.getColumnList().get(0).getName());
            Type idType = table.getColumnList().get(0).getType();

            for (Column column : table.getJoinList()) {
                if (!column.getFkAttribute().equals(table.getName())) continue;

                String tableDtoName = Function.snakeToCamel(column.getFkTable()) + Constant.DTO + configuration.getGenerest().getSuffix();
                String tableDtoAttribute = Function.snakeToLowCamel(column.getFkTable()) + Constant.DTO + configuration.getGenerest().getSuffix();
                String tableEntityName = Function.snakeToCamel(column.getFkTable()) + Constant.ENTITY + configuration.getGenerest().getSuffix();
                String tableEntityAttribute = Function.snakeToLowCamel(column.getFkTable()) + Constant.ENTITY + configuration.getGenerest().getSuffix();
                String tableServiceAttribute = Function.snakeToLowCamel(column.getFkTable()) + Constant.SERVICE + configuration.getGenerest().getSuffix();
                String tableMapperName = Function.snakeToCamel(column.getFkTable()) + Constant.MAPPER + configuration.getGenerest().getSuffix();

                method.add("");
                method.add(TAB_X_1 + "@GetMapping(\"/{" + idName + "}/" + Function.snakeToLowCamel(column.getFkTable()) + "\")");
                if (column.isUnique()) {
                    if (configuration.getDocumentations().isSwagger()) {
                        method.add(TAB_X_1 + "@ApiOperation(value = \"Get " + Function.snakeToLowCamel(column.getFkTable()) + " by " + Function.snakeToLowCamel(column.getFkColumn()) + "\", response = " + dtoName + ".class)");
                        addApiResponse200(dtoFullName, "", method);
                    }
                    method.add(TAB_X_1 + Accessor.PUBLIC.getAccess() + " ResponseEntity<" + tableDtoName + "> get" + Function.snakeToCamel(column.getFkTable()) + "(@PathVariable(\"" + idName + "\") " + idType.getTypeName() + " " + idName + ") {");
                    method.add(TAB_X_2 + "return " + tableServiceAttribute + ".getBy" + Function.snakeToCamel(column.getFkColumn()) + "(" + idName + ")");
                    method.add(TAB_X_2 + TAB_X_2 +".map(entity -> ResponseEntity.ok(" + tableMapperName + ".to" + tableDtoName + "(entity)))");
                    method.add(TAB_X_2 + TAB_X_2 +".orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));");
                    method.add(TAB_X_1 + "}");
                } else {
                    if (configuration.getDocumentations().isSwagger()) {
                        method.add(TAB_X_1 + "@ApiOperation(value = \"Get " + Function.snakeToLowCamel(column.getFkTable()) + "s by " + Function.snakeToLowCamel(column.getFkColumn()) + "\", response = " + dtoName + ".class, responseContainer = \"List\")");
                        addApiResponse200(tableDtoName, ", responseContainer = \"List\"", method);
                    }
                    method.add(TAB_X_1 + Accessor.PUBLIC.getAccess() + " ResponseEntity<List<" + tableDtoName + ">> get" + Function.snakeToCamel(column.getFkTable()) + "s(@PathVariable(\"" + idName + "\") " + idType.getTypeName() + " " + idName + ") {");
                    method.add(TAB_X_2 + "List<" + tableEntityName + "> " + tableEntityAttribute + "List = " + tableServiceAttribute + ".getBy" + Function.snakeToCamel(column.getFkColumn()) + "(" + idName + ");");
                    method.add(TAB_X_2 + "List<" + tableDtoName + "> " + tableDtoAttribute + "List = new ArrayList<>();");
                    method.add(TAB_X_2 + "for (" + tableEntityName + " " + tableEntityAttribute + " : " + tableEntityAttribute + "List) {");
                    method.add(TAB_X_3 + tableDtoAttribute + "List.add(" + tableMapperName + ".to" + tableDtoName + "(" + tableEntityAttribute + "));");
                    method.add(TAB_X_2 + "}");
                    method.add(TAB_X_2 + "return ResponseEntity.ok(" + tableDtoAttribute + "List);");
                    method.add(TAB_X_1 + "}");
                }
            }
        }
    }

    private static void addControllerGetEnum(List<String> method) {
        List<String> endpointPresent = new ArrayList<>();
        for(Column column : table.getColumnList()) {

            if (Type.ENUM.equals(column.getType()) && !endpointPresent.contains(column.getEnumName())) {
                method.add("");
                method.add(TAB_X_1 + "@GetMapping(\"/"+ Function.snakeToLowCamel(column.getEnumName()) +"\")");
                String enumClassName = Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix();

                if (configuration.getDocumentations().isSwagger()) {
                    method.add(TAB_X_1 + "@ApiOperation(value = \"Get " + Function.snakeToLowCamel(column.getEnumName()) + " list\", response = " + enumClassName + ".class, responseContainer = \"List\")");
                    method.add(TAB_X_1 + "@ApiResponses(value = {@ApiResponse(code = 200, message = \"Successful operation\", response = " + enumClassName + ".class, responseContainer = \"List\") })");
                }
                method.add(TAB_X_1 + "public ResponseEntity<List<" + enumClassName + ">> get" + Function.snakeToCamel(column.getEnumName()) + "() {");
                method.add(TAB_X_2 + "List<" + enumClassName + "> " + Function.snakeToLowCamel(enumClassName) + "List = Arrays.asList(" + enumClassName + ".values());");
                method.add(TAB_X_2 + "return ResponseEntity.ok(" + Function.snakeToLowCamel(enumClassName) + "List);");
                method.add(TAB_X_1 + "}");

                endpointPresent.add(column.getEnumName());
            }
        }
    }

    private static void addControllerGetAllMethod(List<String> method) {
        method.add("");
        method.add(TAB_X_1 + "@GetMapping");
        if (configuration.getDocumentations().isSwagger()) {
            method.add(TAB_X_1 + "@ApiOperation(value = \"Get all " + Function.snakeToLowCamel(table.getName()) + "\", response = " + dtoName + ".class, responseContainer = \"List\")");
            addApiResponse200(dtoName, ", responseContainer = \"List\"", method);
        }
        method.add(TAB_X_1 + Accessor.PUBLIC.getAccess() + " ResponseEntity<List<" + dtoName + ">> getAll() {");
        method.add(TAB_X_2 + "List<" + entityName + "> " + entityAttribute + "List = " +  serviceAttribute+ ".getAll();");
        method.add(TAB_X_2 + "List<" + dtoName + "> " + dtoAttribute + "List = new ArrayList<>();");
        method.add(TAB_X_2 + "for (" + entityName + " " + entityAttribute + " : " + entityAttribute + "List) {");
        method.add(TAB_X_3 + dtoAttribute + "List.add(" + mapperName + ".to" + dtoName + "(" + entityAttribute + "));");
        method.add(TAB_X_2 + "}");
        method.add(TAB_X_2 + "return ResponseEntity.ok(" + dtoAttribute + "List);");
        method.add(TAB_X_1 + "}");
    }

    private static void addControllerGetAllWithFilterMethod(List<String> method) {
        method.add("");
        method.add(TAB_X_1 + "@GetMapping(\"/search\")");
        if (configuration.getDocumentations().isSwagger()) {
            method.add(TAB_X_1 + "@ApiOperation(value = \"Get all " + Function.snakeToLowCamel(table.getName()) + " by filters\", response = " + dtoName + ".class, responseContainer = \"List\")");
            addApiResponse200(dtoName, ", responseContainer = \"List\"", method);
        }
        method.add(TAB_X_1 + Accessor.PUBLIC.getAccess() + " ResponseEntity<List<" + dtoName + ">> getAllByFilters(" + filterName + " " + filterAttribute + ") {");
        method.add(TAB_X_2 + "List<" + entityName + "> " + entityAttribute + "List = " +  serviceAttribute+ ".getByFilters(" + filterAttribute + ");");
        method.add(TAB_X_2 + "List<" + dtoName + "> " + dtoAttribute + "List = new ArrayList<>();");
        method.add(TAB_X_2 + "for (" + entityName + " " + entityAttribute + " : " + entityAttribute + "List) {");
        method.add(TAB_X_3 + dtoAttribute + "List.add(" + mapperName + ".to" + dtoName + "(" + entityAttribute + "));");
        method.add(TAB_X_2 + "}");
        method.add(TAB_X_2 + "return ResponseEntity.ok(" + dtoAttribute + "List);");
        method.add(TAB_X_1 + "}");
    }

    private static void addControllerCreateMethod(List<String> method) {
        String idName = Function.snakeToCamel(table.getColumnList().get(0).getName());

        method.add("");
        method.add(TAB_X_1 + "@PostMapping");
        if (configuration.getDocumentations().isSwagger()) {
            method.add(TAB_X_1 + "@ApiOperation(value = \"Create " + Function.snakeToLowCamel(table.getName()) + "\")");
            method.add(TAB_X_1 + "@ApiResponses(value = {@ApiResponse(code = 201, message = \"Successful operation\") })");
        }
        method.add(TAB_X_1 + Accessor.PUBLIC.getAccess() + " ResponseEntity<Void> create(@RequestBody @Valid " + dtoPostName + " " + dtoPostAttribute + ") throws URISyntaxException {");
        method.add(TAB_X_2 + entityName + " " + entityAttribute + " = " + serviceAttribute + ".create(" + mapperName + ".to" + entityName + "(" + dtoPostAttribute + "));");
        method.add(TAB_X_2 + "if (" + entityAttribute + " " + Constant.NOT_NULL + ") {");
        method.add(TAB_X_3 + "return ResponseEntity.created(new URI(\"/" + Function.snakeToLowCamel(table.getName()) + "/\" + " + entityAttribute + ".get" + idName + "())).build();");
        method.add(TAB_X_2 + "}");
        method.add(TAB_X_2 + "return ResponseEntity.status(HttpStatus.CONFLICT).build();");
        method.add(TAB_X_1 + "}");
    }

    private static void addControllerUpdateMethod(List<String> method) {
        Type idType = table.getColumnList().get(0).getType();
        String idName = entityAttribute + Function.snakeToCamel(table.getColumnList().get(0).getName());
        String idField = Function.snakeToCamel(table.getColumnList().get(0).getName());

        method.add("");
        method.add(TAB_X_1 + "@PutMapping(\"/{" + idName + "}\")");
        if (configuration.getDocumentations().isSwagger()) {
            method.add(TAB_X_1 + "@ApiOperation(value = \"Update " + Function.snakeToLowCamel(table.getName()) + "\")");
            method.add(TAB_X_1 + "@ApiResponses(value = {@ApiResponse(code = 204, message = \"Successful operation\") })");
        }
        method.add(TAB_X_1 + Accessor.PUBLIC.getAccess() + " ResponseEntity<Void> update(@PathVariable(\"" + idName + "\") " + idType.getTypeName() + " " + idName + ", @RequestBody @Valid " + dtoPostName + " " + dtoPostAttribute + ") {");
        method.add(TAB_X_2 + "Optional<" + entityName + "> " + entityAttribute + " = " + serviceAttribute + ".get(" + idName + ");");
        method.add(TAB_X_2 + "if (" + entityAttribute + ".isPresent()) {");

        if (configuration.getParameters().isDtoChangeLog()) {
            method.add(TAB_X_3 + entityName + " " + entityAttribute + "New = " + mapperName + ".to" + entityName + "(" + dtoPostAttribute + ", " + entityAttribute + ".get());");
        } else {
            method.add(TAB_X_3 + entityName + " " + entityAttribute + "New = " + mapperName + ".to" + entityName + "(" + dtoPostAttribute + ", " + entityAttribute + ".get().get" + idField + "());");
        }
        method.add(TAB_X_3 + "if (" + serviceAttribute + ".update(" + entityAttribute + "New) " + Constant.NOT_NULL + ") {");
        method.add(TAB_X_2 + TAB_X_2 + "return ResponseEntity.noContent().build();");
        method.add(TAB_X_3 + "}");
        method.add(TAB_X_2 + "}");
        method.add(TAB_X_2 + "return ResponseEntity.notFound().build();");
        method.add(TAB_X_1 + "}");
    }

    private static void addControllerDeleteMethod(List<String> method) {
        Type idType = table.getColumnList().get(0).getType();
        String idName = entityAttribute + Function.snakeToCamel(table.getColumnList().get(0).getName());

        method.add("");
        method.add(TAB_X_1 + "@DeleteMapping(\"/{" + idName + "}\")");
        if (configuration.getDocumentations().isSwagger()) {
            method.add(TAB_X_1 + "@ApiOperation(value = \"Delete " + Function.snakeToLowCamel(table.getName()) + "\")");
            method.add(TAB_X_1 + "@ApiResponses(value = {@ApiResponse(code = 204, message = \"Successful operation\") })");
        }
        method.add(TAB_X_1 + Accessor.PUBLIC.getAccess() + " ResponseEntity<Void> deleteById(@PathVariable(\"" + idName + "\") " + idType.getTypeName() + " " + idName + ") {");
        method.add(TAB_X_2 + "if (" + serviceAttribute + ".deleteById(" + idName + ")) {");
        method.add(TAB_X_3 + "return ResponseEntity.noContent().build();");
        method.add(TAB_X_2 + "}");
        method.add(TAB_X_2 + "return ResponseEntity.notFound().build();");
        method.add(TAB_X_1 + "}");
    }

    private static void addApiResponse200(String responseClass, String responseEnd, List<String> method) {
        method.add(TAB_X_1 + "@ApiResponses(value = {@ApiResponse(code = 200, message = \"Successful operation\", response = " + responseClass + ".class" + responseEnd + ") })");
    }

    private static void generateControllerAttribute(List<String> attributes) {
        List<String> attributePresent = new ArrayList<>();
        attributes.add(TAB_X_1 + Accessor.PRIVATE.getAccess() + " " + serviceName + " " + serviceAttribute + ";");

        if (!table.getJoinList().isEmpty()) {
            for(Column column : table.getJoinList()) {
                if (attributePresent.contains(column.getFkTable())) continue;

                String tableServiceName = Function.snakeToCamel(column.getFkTable()) + Constant.SERVICE + configuration.getGenerest().getSuffix();
                String tableServiceAttribute = Function.snakeToLowCamel(column.getFkTable()) + Constant.SERVICE + configuration.getGenerest().getSuffix();

                attributes.add(TAB_X_1 + Accessor.PRIVATE.getAccess() + " " + tableServiceName + " " + tableServiceAttribute + ";");
                attributePresent.add(column.getFkTable());
            }
        }
    }

    private static void generateControllerSetter(List<String> setters) {
        List<String> setterPresent = new ArrayList<>();

        setters.add("");
        setters.add(TAB_X_1 + "@Autowired");
        generateSetter(serviceName, serviceName, serviceAttribute, setters);

        if (!table.getJoinList().isEmpty()) {
            for(Column column : table.getJoinList()) {
                if (setterPresent.contains(column.getFkTable())) continue;

                String tableServiceName = Function.snakeToCamel(column.getFkTable()) + Constant.SERVICE + configuration.getGenerest().getSuffix();
                String tableServiceAttribute = Function.snakeToLowCamel(column.getFkTable()) + Constant.SERVICE + configuration.getGenerest().getSuffix();

                setters.add("");
                setters.add(TAB_X_1 + "@Autowired");
                generateSetter(tableServiceName, tableServiceName, tableServiceAttribute, setters);
                setterPresent.add(column.getFkTable());
            }
        }
    }

    private static void generateControllerImport() {
        List<String> importPresent = new ArrayList<>();

        content.add(importString + Constant.DTOS + "." + dtoName + ";");
        content.add(importString + Constant.DTOS + ".post." + dtoPostName + ";");
        content.add(importString + Constant.DTOS + ".full." + dtoFullName + ";");
        content.add(importString + Constant.ENTITIES + "." + entityName + ";");
        if (configuration.getGenerated().isFilters()) {
            content.add(importString + Constant.ENTITIES + "." + Constant.FILTERS + "." + filterName + ";");
        }
        content.add(importString + Constant.SERVICES + "." + serviceName + ";");
        content.add(importString + Constant.MAPPERS + "." + mapperName + ";");

        if (!table.getJoinList().isEmpty()) {
            for(Column column : table.getJoinList()) {
                if (importPresent.contains(column.getFkTable())) continue;

                String tableServiceName = Function.snakeToCamel(column.getFkTable()) + Constant.SERVICE + configuration.getGenerest().getSuffix();
                String tableDtoName = Function.snakeToCamel(column.getFkTable()) + Constant.DTO + configuration.getGenerest().getSuffix();
                String tableEntityName = Function.snakeToCamel(column.getFkTable()) + Constant.ENTITY + configuration.getGenerest().getSuffix();
                String tableMapperName = Function.snakeToCamel(column.getFkTable()) + Constant.MAPPER + configuration.getGenerest().getSuffix();

                content.add(importString + Constant.DTOS + "." + tableDtoName + ";");
                content.add(importString + Constant.MAPPERS + "." + tableMapperName + ";");
                content.add(importString + Constant.ENTITIES + "." + tableEntityName + ";");
                content.add(importString + Constant.SERVICES + "." + tableServiceName + ";");
                importPresent.add(column.getFkTable());
            }
        }

        content.add("import org.springframework.beans.factory.annotation.Autowired;");
        content.add("import org.springframework.web.bind.annotation.GetMapping;");
        content.add("import org.springframework.web.bind.annotation.PostMapping;");
        content.add("import org.springframework.web.bind.annotation.PutMapping;");
        content.add("import org.springframework.web.bind.annotation.DeleteMapping;");
        content.add("import org.springframework.web.bind.annotation.RequestMapping;");
        content.add("import org.springframework.web.bind.annotation.RestController;");
        content.add("import org.springframework.web.bind.annotation.RequestBody;");
        content.add("import org.springframework.web.bind.annotation.PathVariable;");
        content.add("import org.springframework.http.ResponseEntity;");
        content.add("import org.springframework.http.HttpStatus;");
        if (configuration.getDocumentations().isSwagger()) {
            content.add("import io.swagger.annotations.Api;");
        }

        if (configuration.getDocumentations().isSwagger()) {
            content.add("");
            content.add("import io.swagger.annotations.ApiOperation;");
            content.add("import io.swagger.annotations.ApiResponse;");
            content.add("import io.swagger.annotations.ApiResponses;");
        }

        content.add("");
        content.add("import java.net.URI;");
        content.add("import javax.validation.Valid;");
        content.add("import java.net.URISyntaxException;");
        content.add("import java.util.ArrayList;");
        content.add("import java.util.Optional;");
        content.add("import java.util.List;");

        generateUUIDImport();

        for (Column column : table.getColumnList()) {
            generateControllerEnumImport(column, importPresent);
        }
    }

    private static void generateControllerEnumImport(Column column, List<String> importPresent) {
        if (Type.ENUM.equals(column.getType()) && !importPresent.contains(column.getEnumName())) {
            if (!importPresent.contains("Arrays")) {
                content.add("import java.util.Arrays;");
                importPresent.add("Arrays");
            }
            generateEnumImport(column, importPresent);
        }
    }
}
