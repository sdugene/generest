package com.sdugene.generator;

import com.sdugene.Constant;
import com.sdugene.Function;
import com.sdugene.enums.Accessor;
import com.sdugene.enums.DataClassType;
import com.sdugene.enums.Type;
import com.sdugene.models.Column;
import com.sdugene.models.Configuration;
import com.sdugene.models.Table;

import java.util.ArrayList;
import java.util.List;

public class DtoGenerator extends Generator {

    private DtoGenerator() {
        throw new IllegalStateException("Utility class");
    }

    public static List<String> generateClass(Table table, Configuration configuration, DataClassType dataClassType) {
        setVariables(table, configuration);
        addHeader();

        String className = Generator.className + dataClassType.getName() + dataClassType.getSuffix() + configuration.getGenerest().getSuffix();

        content.add("");
        if (DataClassType.DTO.equals(dataClassType)) {
            content.add("package" + " " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.DTOS + ";");

            content.add("");
            generateDtoImport(dataClassType);

        } else {
            String specificPackage = "." + dataClassType.getName().toLowerCase();
            content.add("package" + " " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.DTOS + specificPackage + ";");

            content.add("");
            generateDtoImport(dataClassType);

            if (configuration.getParameters().isDtoChangeLog()) {
                content.add("import" + " " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.DTOS + ".Dto;");
            }
        }
        if (configuration.getParameters().isLombok()) {
            generateLombokImport(dataClassType);
        }

        content.add("");
        if (configuration.getParameters().isLombok()) {
            generateLombokAnnotation(dataClassType);
        }

        if (configuration.getParameters().isDtoChangeLog()) {
            content.add(Accessor.PUBLIC.getAccess() + " class " + className + " extends Dto {");
        } else {
            content.add(Accessor.PUBLIC.getAccess() + " class " + className + " {");
        }

        // Attributes
        List<String> attributes = new ArrayList<>();
        generateDtoAttribute(attributes, dataClassType);

        List<String> constructors = new ArrayList<>();
        if (!configuration.getParameters().isLombok()) {
            generateClassEmptyConstructorMethod(constructors, dataClassType);
            generateClassAllArgsConstructorMethod(constructors, dataClassType);
            generateClassConstructorCopyMethod(constructors, dataClassType);
        }

        // Setters & Getters
        List<String> setters = new ArrayList<>();
        List<String> getters = new ArrayList<>();
        List<String> methods = new ArrayList<>();

        if (DataClassType.DTO_POST.equals(dataClassType) && configuration.getParameters().isDtoChangeLog()) {
            generateDtoSetter(setters, dataClassType);
        }

        if (!configuration.getParameters().isLombok()) {
            generateDtoGetter(getters, dataClassType);
            generateToStringMethod(dataClassType, methods, false);
        }

        generateClassContent(attributes, constructors, setters, getters, methods);

        if (!configuration.getParameters().isLombok()) {
            generateBuilder(dataClassType);
        }

        generateEquals(dataClassType);

        content.add("}");
        return content;
    }

    private static void generateDtoSetter(List<String> setters, DataClassType dataClassType) {
        for (Column column : table.getColumnList()) {
            if (DataClassType.DTO_POST.equals(dataClassType) && column.isPrimary()) continue;

            setters.add("");
            if (!DataClassType.DTO.equals(dataClassType) && column.getConstraintName() != null) {
                generateSetter(column.getType().getTypeName(), column.getName(), Function.snakeToCamel(column.getName()), Function.snakeToLowCamel(column.getName()), setters);
            } else if (Type.ENUM.equals(column.getType())) {
                generateSetter(Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix(), column.getName(), setters);
            } else if (Type.JSONB.equals(column.getType())) {
                generateSetter(Type.STRING.getTypeName(), column.getName(), setters);
            } else if (dates.contains(column.getType())) {
                generateSetter(getDateType(column.getType()).getTypeName(), column.getName(), setters);
            } else {
                generateStandardSetter(column, setters);
            }
        }
    }

    private static void generateSetter(String type, String columnName, List<String> setters) {
        String name = Function.snakeToCamel(columnName);
        String attribute = Function.snakeToLowCamel(columnName);
        generateSetter(type, columnName,  name, attribute, setters);
    }

    private static void generateSetter(String type, String columnName, String name, String attribute, List<String> setters) {
        setters.add("\t" + Accessor.PUBLIC.getAccess() + " void set" + name + "(" + type + " " + attribute + ") {");
        setters.add("\t\tthis." + attribute + " = " + attribute + ";");
        setters.add("\t\tthis.changeLog.add(Attribute" + configuration.getGenerest().getSuffix() + "." + columnName.toUpperCase() + ");");
        setters.add("\t}");
    }

    private static void generateStandardSetter(Column column, List<String> setters) {
        generateSetter(column.getType().getTypeName(), column.getName(), setters);
    }

    private static void generateDtoGetter(List<String> getters, DataClassType dataClassType) {
        for (Column column : table.getColumnList()) {
            if (DataClassType.DTO_POST.equals(dataClassType) && column.isPrimary()) continue;
            getters.add("");
            if (dataClassType.hasForeignKey() && column.getConstraintName() != null) {
                generateGetter(Function.snakeToCamel(column.getFkTable()) + Constant.DTO + configuration.getGenerest().getSuffix(), Function.snakeToCamel(column.getFkAttribute()), Function.snakeToLowCamel(column.getFkAttribute()), getters);
            } else if (Type.ENUM.equals(column.getType())) {
                generateGetter(Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix(), Function.snakeToCamel(column.getName()), Function.snakeToLowCamel(column.getName()), getters);
            } else if (Type.JSONB.equals(column.getType())) {
                generateGetter(Type.STRING.getTypeName(), Function.snakeToCamel(column.getName()), Function.snakeToLowCamel(column.getName()), getters);
            } else if (dates.contains(column.getType())) {
                generateGetter(getDateType(column.getType()).getTypeName(), column.getName(), getters);
            }  else {
                generateStandardGetter(column, getters);
            }
        }
    }

    private static void generateGetter(String type, String columnName, List<String> getters) {
        String name = Function.snakeToCamel(columnName);
        String attribute = Function.snakeToLowCamel(columnName);
        generateGetter(type, name, attribute, getters);
    }

    private static void generateDtoAttribute(List<String> attributes, DataClassType dataClassType) {
        attributes.add("");
        for (Column column : table.getColumnList()) {
            if (DataClassType.DTO_POST.equals(dataClassType) && column.isPrimary()) continue;
            if (dataClassType.hasForeignKey() && column.getConstraintName() != null) {
                attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + Function.snakeToCamel(column.getFkTable()) + Constant.DTO + configuration.getGenerest().getSuffix() +" " + Function.snakeToLowCamel(column.getFkAttribute()) + ";");
            } else if (Type.ENUM.equals(column.getType())) {
                attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix() +" " + Function.snakeToLowCamel(column.getName()) + ";");
            } else if (Type.JSONB.equals(column.getType())) {
                attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + Type.STRING.getTypeName() +" " + Function.snakeToLowCamel(column.getName()) + ";");
            } else if (dates.contains(column.getType())) {
                attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + getDateType(column.getType()).getTypeName() + " " + Function.snakeToLowCamel(column.getName()) + ";");
            } else {
                attributes.add("\t" + Accessor.PRIVATE.getAccess() + " " + column.getType().getTypeName() + " " + Function.snakeToLowCamel(column.getName()) + ";");
            }
        }
    }

    public static List<String> generateParentClass(Configuration configuration) {
        setVariables(null, configuration);
        addHeader();

        content.add("package " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.DTOS + ";");

        content.add("");
        content.add(importString + Constant.ENUMS + ".IAttribute" + configuration.getGenerest().getSuffix() + ";");
        content.add("import java.util.ArrayList;");
        content.add("import java.util.List;");

        content.add("");
        content.add(Accessor.PUBLIC.getAccess() + " abstract class Dto {");

        // Attributes
        List<String> attributes = new ArrayList<>();
        attributes.add("\t" + Accessor.PROTECTED.getAccess() + " List<IAttribute" + configuration.getGenerest().getSuffix() +"> changeLog;");

        // Constructor
        List<String> constructors = new ArrayList<>();
        constructors.add("");
        constructors.add("\t" + Accessor.PUBLIC.getAccess() + " Dto() {");
        constructors.add("\t\tchangeLog = new ArrayList<>();");
        constructors.add("\t}");

        // Methods
        List<String> methods = new ArrayList<>();
        methods.add("");
        methods.add("\t" + Accessor.PUBLIC.getAccess() + " boolean isChanged(IAttribute" + configuration.getGenerest().getSuffix() + " attribute) {");
        methods.add("\t\treturn changeLog.contains(attribute);");
        methods.add("\t}");

        content.add("");
        generateClassContent(attributes, constructors, methods);

        content.add("}");
        return content;
    }

    private static void generateDtoImport(DataClassType dataClassType) {
        content.add("import java.util.Objects;");
        List<String> importPresent = new ArrayList<>();

        if (DataClassType.DTO_POST.equals(dataClassType) && configuration.getParameters().isDtoChangeLog()) {
            content.add("import " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.ENUMS + ".Attribute" + configuration.getGenerest().getSuffix() + ";");
        }

        for (Column column : table.getColumnList()) {
            if (DataClassType.DTO_POST.equals(dataClassType) && column.isPrimary()) continue;

            if (dataClassType.hasForeignKey() && column.getConstraintName() != null) {
                generateDtoFKImport(column, importPresent);
            } else {
                generateTypeImport(column, importPresent, true);
                generateEnumImport(column, importPresent);
            }
        }
    }

    private static void generateDtoFKImport(Column column, List<String> importPresent) {
        String tableDtoName = Function.snakeToCamel(column.getFkTable()) + Constant.DTO + configuration.getGenerest().getSuffix();
        if (!importPresent.contains(tableDtoName)) {
            content.add("import " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.DTOS + "." + tableDtoName + ";");
            importPresent.add(tableDtoName);
        }
    }
}
