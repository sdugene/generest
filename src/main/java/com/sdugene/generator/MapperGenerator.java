package com.sdugene.generator;

import com.sdugene.Constant;
import com.sdugene.Function;
import com.sdugene.enums.Accessor;
import com.sdugene.enums.DataClassType;
import com.sdugene.enums.Type;
import com.sdugene.models.Column;
import com.sdugene.models.Configuration;
import com.sdugene.models.Table;

import java.util.ArrayList;
import java.util.List;

import static com.sdugene.Constant.*;

public class MapperGenerator extends Generator {

    private MapperGenerator() {
        throw new IllegalStateException("Utility class");
    }

    public static List<String> generateClass(Table table, Configuration configuration) {
        setVariables(table, configuration);
        addMapperHeaders();

        content.add("");
        generateMapperImport();

        content.add("");
        content.add(Accessor.PUBLIC.getAccess() + " class " + mapperName + " {");

        // Constructor
        List<String> constructors = new ArrayList<>();
        constructors.add("\t" + Accessor.PRIVATE.getAccess() + " " + mapperName + "() {");
        constructors.add("\t\tthrow new IllegalStateException(\"Utility class\");");
        constructors.add("\t}");

        // Methods
        List<String> methods = new ArrayList<>();
        addSourceToTargetMethod(methods, DataClassType.ENTITY, DataClassType.DTO);
        addSourceToTargetMethod(methods, DataClassType.ENTITY, DataClassType.DTO_FULL);

        if (!table.isView()) {
            addSourceToTargetMethod(methods, DataClassType.DTO, DataClassType.ENTITY);
            addSourceToTargetMethod(methods, DataClassType.DTO_POST, DataClassType.ENTITY);

            if (configuration.getParameters().isDtoChangeLog()) {
                addDtoToEntityMethod(table, methods);
            } else {
                addDtoAndIdToEntityMethod(table, methods);
            }
        }

        content.add("");
        generateClassContent(constructors, methods);

        content.add("}");
        return content;
    }

    private static void addSourceToTargetMethod(List<String> methods, DataClassType sourceDataClassType, DataClassType targetDataClassType) {
        String sourceName = className + sourceDataClassType.getComplete() + configuration.getGenerest().getSuffix();
        String targetName = className + targetDataClassType.getComplete() + configuration.getGenerest().getSuffix();

        methods.add("");
        String sourceAttribute = Function.snakeToLowCamel(sourceName);

        StringBuilder arguments = new StringBuilder();
        arguments.append(sourceName).append(" ").append(sourceAttribute);
        if (!sourceDataClassType.hasForeignKey()) {
            addMethodFkArguments(table, targetDataClassType, arguments);
        }

        methods.add(TAB_X_1 + Accessor.PUBLIC.getAccess() + " static " + targetName + " to" + targetName + "(" + arguments + ") {");
        methods.add(TAB_X_2 + "if (" + sourceAttribute + " == null) return null;");
        methods.add(TAB_X_2 + "return " + targetName + ".builder()");

        for (Column column : table.getColumnList()) {
            if ((targetDataClassType.getSuffix().equals(ENTITY) && column.isPrimary())
                    || (!sourceDataClassType.hasPrimaryKey() && column.isPrimary())) continue;

            String columnAttribute = Function.snakeToLowCamel(column.getName());

            if (column.getConstraintName() != null) {
                addFkMapping(methods, sourceDataClassType, targetDataClassType, sourceAttribute, column, columnAttribute);
            } else {
                methods.add(TAB_X_3 + "." + columnAttribute + "(" + sourceAttribute + ".get" + Function.snakeToCamel(columnAttribute) + "())");
            }
        }
        methods.add(TAB_X_3 + ".build();");
        methods.add(TAB_X_1 + "}");
    }

    private static void addFkMapping(List<String> methods, DataClassType sourceType, DataClassType targetType, String sourceAttribute, Column column, String columnAttribute) {
        String fkAttribute = Function.snakeToCamel(column.getFkAttribute());
        String fkColumn = Function.snakeToCamel(column.getFkColumn());

        if (targetType.hasForeignKey()) {
            String fkTable = Function.snakeToCamel(column.getFkTable() + targetType.getSuffix() + configuration.getGenerest().getSuffix());
            String columnTargetAttribute = Function.snakeToLowCamel(column.getFkAttribute());

            if (sourceType.hasForeignKey()) {
                if (Boolean.TRUE.equals(column.isNullable())) {
                    methods.add(TAB_X_3 + "." + columnTargetAttribute + "(" + sourceAttribute + ".get" + fkAttribute + "() " + NOT_NULL + " ? " + Function.snakeToCamel(column.getFkTable()) + "MapperGen.to" + fkTable + "(" + sourceAttribute + ".get" + fkAttribute + "()) : null)");
                } else {
                    methods.add(TAB_X_3 + "." + columnTargetAttribute + "(" + Function.snakeToCamel(column.getFkTable()) + "MapperGen.to" + fkTable + "(" + sourceAttribute + ".get" + fkAttribute + "()))");
                }
            } else {
                if (Boolean.TRUE.equals(column.isNullable())) {
                    methods.add(TAB_X_3 + "." + columnTargetAttribute + "(" + sourceAttribute + ".get" + Function.snakeToCamel(columnAttribute) + "() " + NOT_NULL + " ? " + Function.snakeToLowCamel(fkAttribute) + " : null)");
                } else {
                    methods.add(TAB_X_3 + "." + columnTargetAttribute + "(" + Function.snakeToLowCamel(fkAttribute) +")");

                }
            }
        } else {
            if (Boolean.TRUE.equals(column.isNullable())) {
                methods.add(TAB_X_3 + "." + columnAttribute + "(" + sourceAttribute + ".get" + fkAttribute + "() " + NOT_NULL + " ? " + sourceAttribute + ".get" + fkAttribute + "().get" + fkColumn + "() : null)");
            } else {
                methods.add(TAB_X_3 + "." + columnAttribute + "(" + sourceAttribute + ".get" + fkAttribute + "().get" + fkColumn + "())");
            }
        }
    }

    private static void addDtoToEntityMethod(Table table, List<String> methods) {
        DataClassType targetType = DataClassType.ENTITY;

        methods.add("");

        StringBuilder arguments = new StringBuilder();
        arguments.append(dtoPostName).append(" ").append(dtoPostAttribute).append(", ").append(entityName).append(" ").append(entityAttribute);
        addMethodFkArguments(table, targetType, arguments);

        methods.add("\t" + Accessor.PUBLIC.getAccess() + " static " + entityName + " to" + entityName + "(" + arguments + ") {");
        for (Column column : table.getColumnList()) {
            if (column.isPrimary()) continue;

            if (column.getConstraintName() != null) {
                methods.add("\t\tif (" + dtoPostAttribute + ".isChanged(Attribute" + configuration.getGenerest().getSuffix() + "." + column.getName().toUpperCase() + ")) {");
                methods.add("\t\t\t" + entityAttribute + ".set" + Function.snakeToCamel(column.getFkAttribute()) + "(" + Function.snakeToLowCamel(column.getFkAttribute()) + ");");
            } else {
                methods.add("\t\tif (" + dtoPostAttribute + ".isChanged(Attribute" + configuration.getGenerest().getSuffix() + "." + column.getName().toUpperCase() + ")) {");
                methods.add("\t\t\t" + entityAttribute + ".set" + Function.snakeToCamel(column.getName()) + "(" + dtoPostAttribute + ".get" + Function.snakeToCamel(column.getName()) + "());");
            }
            methods.add("\t\t}");
        }
        methods.add("\t\treturn " + entityAttribute + ";");
        methods.add("\t}");
    }

    private static void addMethodFkArguments(Table table, DataClassType targetType, StringBuilder arguments) {
        for (Column column : table.getColumnList()) {
             if (column.getConstraintName() != null) {
                 String fkTable = Function.snakeToCamel(column.getFkTable() + targetType.getSuffix() + configuration.getGenerest().getSuffix());
                 String fkAttribute = Function.snakeToLowCamel(column.getFkAttribute());
                 arguments.append(", ").append(fkTable).append(" ").append(fkAttribute);
             }
         }
    }

    private static void addDtoAndIdToEntityMethod(Table table, List<String> methods) {
        DataClassType sourceType = DataClassType.DTO_POST;
        DataClassType targetType = DataClassType.ENTITY;

        Type idType = table.getColumnList().get(0).getType();
        String idName = entityAttribute + Function.snakeToCamel(table.getColumnList().get(0).getName());

        String sourceName = className + sourceType.getComplete() + configuration.getGenerest().getSuffix();
        String targetName = className + targetType.getComplete() + configuration.getGenerest().getSuffix();

        String sourceAttribute = Function.snakeToLowCamel(sourceName);

        methods.add("");
        methods.add("\t" + Accessor.PUBLIC.getAccess() + " static " + entityName + " to" + entityName + "(" + dtoPostName + " " + dtoPostAttribute + ", " + idType + " " + idName + ") {");

        methods.add(TAB_X_2 + "if (" + sourceAttribute + " == null) return null;");
        methods.add(TAB_X_2 + "return " + targetName + ".builder()");

        for (Column column : table.getColumnList()) {
            String columnAttribute = Function.snakeToLowCamel(column.getName());
            if (column.isPrimary()) {
                methods.add(TAB_X_3 + "." + columnAttribute + "(" + idName + ")");
                continue;
            }

            if (column.getConstraintName() != null) {
                addFkMapping(methods, sourceType, targetType, sourceAttribute, column, columnAttribute);
            } else {
                methods.add(TAB_X_3 + "." + columnAttribute + "(" + sourceAttribute + ".get" + Function.snakeToCamel(columnAttribute) + "())");
            }
        }
        methods.add(TAB_X_3 + ".build();");
        methods.add(TAB_X_1 + "}");
    }

    private static void addMapperHeaders() {
        addHeader();

        content.add("");
        content.add("package " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.MAPPERS + ";");
    }

    private static void generateMapperImport() {
        List<String> importPresent = new ArrayList<>();
        if (!table.isView()) {
            if (configuration.getParameters().isDtoChangeLog()) {
                content.add(importString + Constant.ENUMS + ".Attribute" + configuration.getGenerest().getSuffix() + ";");
            } else {
                generateUUIDImport();
            }
        }
        content.add(importString + Constant.ENTITIES + "." + entityName + ";");
        content.add(importString + Constant.DTOS + "." + dtoName + ";");
        if (!table.isView()) {
            content.add(importString + Constant.DTOS + ".post." + dtoPostName + ";");
        }
        content.add(importString + Constant.DTOS + ".full." + dtoFullName + ";");

        for (Column column : table.getColumnList()) {
            if (column.getConstraintName() != null) {
                generateDtoFKImport(column, importPresent);
            }
        }
    }

    private static void generateDtoFKImport(Column column, List<String> importPresent) {
        String entityName = Function.snakeToCamel(column.getFkTable()) + Constant.ENTITY + configuration.getGenerest().getSuffix();
        if (!importPresent.contains(entityName)) {
            content.add("import " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.ENTITIES + "." + entityName + ";");
            importPresent.add(entityName);
        }
    }
}
