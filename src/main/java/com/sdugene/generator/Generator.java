package com.sdugene.generator;

import com.sdugene.Constant;
import com.sdugene.Function;
import com.sdugene.enums.Accessor;
import com.sdugene.enums.DataClassType;
import com.sdugene.enums.Type;
import com.sdugene.models.Column;
import com.sdugene.models.Configuration;
import com.sdugene.models.Table;
import org.apache.maven.shared.utils.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.sdugene.Constant.*;

abstract class Generator {

    private static final String COMMENT_SEPARATOR = " **************************************************";

    static String className;
    static String classAttribute;
    static String entityName;
    static String entityAttribute;
    static String filterName;
    static String filterAttribute;
    static String repositoryName;
    static String repositoryAttribute;
    static String repositorySpecificationsName;
    static String repositorySpecificationsAttribute;
    static String serviceName;
    static String serviceAttribute;
    static String dtoName;
    static String dtoAttribute;
    static String dtoPostName;
    static String dtoPostAttribute;
    static String dtoFullName;
    static String dtoFullAttribute;
    static String mapperName;
    static String controllerName;

    static List<Type> dates = Arrays.asList(Type.LOCAL_DATE_TIME, Type.LOCAL_DATE, Type.LOCAL_TIME, Type.TIMESTAMP);

    static Configuration configuration;
    static Table table;
    static List<String> content;
    static String importString;
    static String importStaticString;

    Generator() {
        throw new IllegalStateException("Utility class");
    }

    static Type getDateType(Type type) {
        if (type.equals(Type.TIMESTAMP)) {
            return configuration.getParameters().getTimestampType();
        } else {
            return type;
        }
    }

    static void setVariables(Table table, Configuration configuration) {
        Generator.configuration = configuration;
        Generator.table = table;
        Generator.content = new ArrayList<>();
        Generator.importString = "import " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted();
        Generator.importStaticString = "import static " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted();

        if (table != null) {
            Generator.className = Function.snakeToCamel(table.getName());
            Generator.classAttribute = Function.snakeToLowCamel(table.getName());

            Generator.entityName = Function.snakeToCamel(table.getName()) + Constant.ENTITY + configuration.getGenerest().getSuffix();
            Generator.entityAttribute = Function.snakeToLowCamel(table.getName()) + Constant.ENTITY + configuration.getGenerest().getSuffix();

            Generator.filterName = Function.snakeToCamel(table.getName()) + Constant.FILTER + configuration.getGenerest().getSuffix();
            Generator.filterAttribute = Function.snakeToLowCamel(table.getName()) + Constant.FILTER + configuration.getGenerest().getSuffix();

            Generator.repositoryName = Function.snakeToCamel(table.getName()) + Constant.REPOSITORY + configuration.getGenerest().getSuffix();
            Generator.repositoryAttribute = Function.snakeToLowCamel(table.getName()) + Constant.REPOSITORY + configuration.getGenerest().getSuffix();

            Generator.repositorySpecificationsName = Function.snakeToCamel(table.getName()) + StringUtils.capitalizeFirstLetter(Constant.SPECIFICATIONS) + configuration.getGenerest().getSuffix();
            Generator.repositorySpecificationsAttribute = Function.snakeToLowCamel(table.getName()) + StringUtils.capitalizeFirstLetter(Constant.SPECIFICATIONS) + configuration.getGenerest().getSuffix();

            Generator.serviceName = Function.snakeToCamel(table.getName()) + Constant.SERVICE + configuration.getGenerest().getSuffix();
            Generator.serviceAttribute = Function.snakeToLowCamel(table.getName()) + Constant.SERVICE + configuration.getGenerest().getSuffix();

            Generator.dtoName = Function.snakeToCamel(table.getName()) + Constant.DTO + configuration.getGenerest().getSuffix();
            Generator.dtoAttribute = Function.snakeToLowCamel(table.getName()) + Constant.DTO + configuration.getGenerest().getSuffix();

            Generator.dtoPostName = Function.snakeToCamel(table.getName()) + "Post" + Constant.DTO + configuration.getGenerest().getSuffix();
            Generator.dtoPostAttribute = Function.snakeToLowCamel(table.getName()) + "Post" + Constant.DTO + configuration.getGenerest().getSuffix();

            Generator.dtoFullName = Function.snakeToCamel(table.getName()) + "Full" + Constant.DTO + configuration.getGenerest().getSuffix();
            Generator.dtoFullAttribute = Function.snakeToLowCamel(table.getName()) + "Full" + Constant.DTO + configuration.getGenerest().getSuffix();

            Generator.mapperName = Function.snakeToCamel(table.getName()) + Constant.MAPPER + configuration.getGenerest().getSuffix();

            Generator.controllerName = Function.snakeToCamel(table.getName()) + Constant.CONTROLLER + configuration.getGenerest().getSuffix();
        }
    }

    static void addHeader() {
        content.add("/*");
        content.add(COMMENT_SEPARATOR);
        content.add(" * @generator      : GeneRest");
        content.add(" * @url            : www.generest.fr");
        content.add(COMMENT_SEPARATOR);
        content.add("*/");
    }

    static void generateUUIDImport() {
        if (table.getColumnList()
                .stream()
                .anyMatch(column -> column.isPrimary(table.getColumnList())
                        && Type.UUID.equals(column.getType()))) {
            content.add("import java.util.UUID;");
            return;
        }

        if (Type.UUID.equals(table.getColumnList().get(0).getType())) {
            content.add("import java.util.UUID;");
        }
    }

    static void generateTypeImport(Column column, List<String> importPresent, boolean equals) {
        if (Type.UUID.equals(column.getType()) && !importPresent.contains(Type.UUID.getTypeName())) {
            content.add("import java.util.UUID;");
            importPresent.add(Type.UUID.getTypeName());
        }

        if (Type.BIG_DECIMAL.equals(column.getType()) && !importPresent.contains(Type.BIG_DECIMAL.getTypeName())) {
            content.add("import java.math.BigDecimal;");
            importPresent.add(Type.BIG_DECIMAL.getTypeName());
        }

        if (column.getType().equals(Type.LOCAL_TIME)
                && !importPresent.contains(Type.LOCAL_TIME.getTypeName())) {
            content.add("import java.time.LocalTime;");
            importPresent.add(Type.LOCAL_TIME.getTypeName());
        }

        if (column.getType().equals(Type.LOCAL_DATE)
                && !importPresent.contains(Type.LOCAL_DATE.getTypeName())) {
            content.add("import java.time.LocalDate;");
            importPresent.add(Type.LOCAL_DATE.getTypeName());
        }

        if (column.getType().equals(Type.LOCAL_DATE_TIME)
                && !importPresent.contains(Type.LOCAL_DATE_TIME.getTypeName())) {
            content.add("import java.time.LocalDateTime;");
            importPresent.add(Type.LOCAL_DATE_TIME.getTypeName());
        }

        if (column.getType().equals(Type.TIMESTAMP)) {
            if (configuration.getParameters().getTimestampType().equals(Type.LOCAL_TIME)
                    && !importPresent.contains(Type.LOCAL_TIME.getTypeName())) {
                content.add("import java.time.LocalTime;");
                importPresent.add(Type.LOCAL_TIME.getTypeName());
            } else if (configuration.getParameters().getTimestampType().equals(Type.LOCAL_DATE)
                    && !importPresent.contains(Type.LOCAL_DATE.getTypeName())) {
                content.add("import java.time.LocalDate;");
                importPresent.add(Type.LOCAL_DATE.getTypeName());
            } else if (configuration.getParameters().getTimestampType().equals(Type.TIMESTAMP)
                    && !importPresent.contains(Type.TIMESTAMP.getTypeName())) {
                content.add("import java.sql.Timestamp;");
                importPresent.add(Type.TIMESTAMP.getTypeName());
            } else if (configuration.getParameters().getTimestampType().equals(Type.LOCAL_DATE_TIME)
                    && !importPresent.contains(Type.LOCAL_DATE_TIME.getTypeName())) {
                content.add("import java.time.LocalDateTime;");
                importPresent.add(Type.LOCAL_DATE_TIME.getTypeName());
            }
        }

        if (dates.contains(column.getType())
                && !importPresent.contains(Type.TIMESTAMP.getTypeName())
                && equals) {
            content.add("import java.sql.Timestamp;");
            importPresent.add(Type.TIMESTAMP.getTypeName());
        }
    }

    static void generateEnumImport(Column column, List<String> importPresent) {
        if (Type.ENUM.equals(column.getType()) && !importPresent.contains(column.getEnumName())) {
            content.add("import " + configuration.getJava().getDefaultPackageDotted()
                + configuration.getGenerest().getPackageNameDoted() + Constant.ENUMS
                + "." + Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix() + ";");
            importPresent.add(column.getEnumName());
        }
    }

    @SafeVarargs
    static void generateClassContent(List<String>... parts) {
        for (List<String> part : parts) {
            content.addAll(part);
        }
    }

    static void generateSetter(List<String> setters, DataClassType dataClassType) {
        for (Column column : table.getColumnList()) {
            setters.add("");
            if (column.getConstraintName() != null && dataClassType.hasForeignKey()) {
                generateSetter(Function.snakeToCamel(column.getFkTable()) + dataClassType.getSuffix() + configuration.getGenerest().getSuffix(), Function.snakeToCamel(column.getFkAttribute()), Function.snakeToLowCamel(column.getFkAttribute()), setters);
            } else if (Type.ENUM.equals(column.getType())) {
                generateSetter(Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix(), Function.snakeToCamel(column.getName()), Function.snakeToLowCamel(column.getName()), setters);
            }  else if (Type.JSONB.equals(column.getType())) {
                generateSetter(Type.STRING.getTypeName(), Function.snakeToCamel(column.getName()), Function.snakeToLowCamel(column.getName()), setters);
            } else if (dates.contains(column.getType())) {
                generateSetter(getDateType(column.getType()).getTypeName(), Function.snakeToCamel(column.getName()), Function.snakeToLowCamel(column.getName()), setters);
            } else {
                generateStandardSetter(column, setters);
            }
        }
    }

    static void generateGetter(List<String> getters, DataClassType dataClassType) {
        for (Column column : table.getColumnList()) {
            getters.add("");
            if (column.getConstraintName() != null && dataClassType.hasForeignKey()) {
                generateGetter(Function.snakeToCamel(column.getFkTable()) + dataClassType.getSuffix() + configuration.getGenerest().getSuffix(), Function.snakeToCamel(column.getFkAttribute()), Function.snakeToLowCamel(column.getFkAttribute()), getters);
            } else if (Type.ENUM.equals(column.getType())) {
                generateGetter(Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix(), Function.snakeToCamel(column.getName()), Function.snakeToLowCamel(column.getName()), getters);
            } else if (Type.JSONB.equals(column.getType())) {
                generateGetter(Type.STRING.getTypeName(), Function.snakeToCamel(column.getName()), Function.snakeToLowCamel(column.getName()), getters);
            } else if (dates.contains(column.getType())) {
                generateGetter(getDateType(column.getType()).getTypeName(), Function.snakeToCamel(column.getName()), Function.snakeToLowCamel(column.getName()), getters);
            } else {
                generateStandardGetter(column, getters);
            }
        }
    }

    private static void generateStandardSetter(Column column, List<String> setters) {
        generateSetter(column.getType().getTypeName(), Function.snakeToCamel(column.getName()), Function.snakeToLowCamel(column.getName()), setters);
    }

    static void generateStandardGetter(Column column, List<String> getters) {
        generateGetter(column.getType().getTypeName(), Function.snakeToCamel(column.getName()), Function.snakeToLowCamel(column.getName()), getters);
    }

    static void generateGetter(String type, String name, String attribute, List<String> getters) {
        getters.add("\t" + Accessor.PUBLIC.getAccess() + " " + type + " get" + name + "() {");
        getters.add("\t\treturn " + attribute + ";");
        getters.add("\t}");
    }

    static void generateSetter(String type, String name, String attribute, List<String> setters) {
        setters.add("\t" + Accessor.PUBLIC.getAccess() + " void set" + name + "(" + type + " " + attribute + ") {");
        setters.add(TAB_X_2_THIS + "." + attribute + " = " + attribute + ";");
        setters.add("\t}");
    }

    static void generateAutowiredSetter(String name, String attribute, String className, List<String> setters) {
        setters.add("");
        setters.add("\t@Autowired");
        setters.add("\t" + Accessor.PUBLIC.getAccess() + " " + className + " set" + name + "(" + name + " " + attribute + ") {");
        setters.add(TAB_X_2_THIS + "." + attribute + " = " + attribute + ";");
        setters.add("\t\treturn this;");
        setters.add("\t}");
    }

    static void generateBuilder(DataClassType dataClassType) {
        content.add("");

        content.add("\t" + Accessor.PUBLIC.getAccess() + " static " + className + dataClassType.getName() + dataClassType.getSuffix() + BUILDER + " builder() {");
        content.add("\t\t return new " + className + dataClassType.getName() + dataClassType.getSuffix() + BUILDER + "();");
        content.add("\t}");

        content.add("");

        content.add("\t" + Accessor.PUBLIC.getAccess() + " static class " + className + dataClassType.getName() + dataClassType.getSuffix() + BUILDER + " {");

        generateBuilderAttribute(dataClassType);
        generateBuilderConstructor(dataClassType);
        generateBuilderMethods(dataClassType);

        content.add("\t}");
    }

    static void generateEquals(DataClassType dataClassType) {
        String name = className + dataClassType.getName() + dataClassType.getSuffix() + configuration.getGenerest().getSuffix();

        content.add("");
        content.add("\t@Override");
        content.add("\t" + Accessor.PUBLIC.getAccess() + " boolean equals(Object o) {");
        content.add("\t\tif (this == o) return true;");
        content.add("\t\tif (!(o instanceof " + name + ")) return false;");
        content.add("\t\tfinal " + name + " other = (" + name + ") o;");
        content.add("\t\tif (!other.canEqual((Object) this)) return false;");

        String columnType;
        String thisAttribute;
        String otherAttribute;
        for (Column column : table.getColumnList()) {
            if (!dataClassType.hasPrimaryKey() && column.isPrimary()) continue;
            if (column.getConstraintName() != null && dataClassType.hasForeignKey()) {
                columnType = Function.snakeToCamel(column.getFkTable())
                        + dataClassType.getSuffix()
                        + configuration.getGenerest().getSuffix();

                thisAttribute = "this" + Function.snakeToCamel(column.getFkAttribute());
                otherAttribute = "other" + Function.snakeToCamel(column.getFkAttribute());
                String getter = "get" + Function.snakeToCamel(column.getFkAttribute()) + "()";

                content.add("\t\tfinal " + columnType + " " + thisAttribute + " = this." + getter + ";");
                content.add("\t\tfinal " + columnType + " " + otherAttribute + " = other." + getter + ";");
            } else if (dates.contains(column.getType())) {
                columnType = Type.LONG.getTypeName();

                thisAttribute = "this" + Function.snakeToCamel(column.getName());
                otherAttribute = "other" + Function.snakeToCamel(column.getName());
                String getter = "get" + Function.snakeToCamel(column.getName()) + "()";

                String thisGetterTime;
                String OtherGetterTime;
                if (getDateType(column.getType()).equals(Type.LOCAL_DATE)) {
                    thisGetterTime = "Timestamp.valueOf(this.get" + Function.snakeToCamel(column.getName()) + "().atStartOfDay()).getTime()";
                    OtherGetterTime = "Timestamp.valueOf(this.get" + Function.snakeToCamel(column.getName()) + "().atStartOfDay()).getTime()";
                } else if (getDateType(column.getType()).equals(Type.LOCAL_TIME)) {
                    thisGetterTime = "Timestamp.valueOf(this.get" + Function.snakeToCamel(column.getName()) + "().atDate(LocalDate.now())).getTime()";
                    OtherGetterTime = "Timestamp.valueOf(this.get" + Function.snakeToCamel(column.getName()) + "().atDate(LocalDate.now())).getTime()";
                } else if (getDateType(column.getType()).equals(Type.TIMESTAMP)) {
                    thisGetterTime = "this.get" + Function.snakeToCamel(column.getName()) + "().getTime()";
                    OtherGetterTime = "this.get" + Function.snakeToCamel(column.getName()) + "().getTime()";
                } else {
                    thisGetterTime = "Timestamp.valueOf(this.get" + Function.snakeToCamel(column.getName()) + "()).getTime()";
                    OtherGetterTime = "Timestamp.valueOf(this.get" + Function.snakeToCamel(column.getName()) + "()).getTime()";
                }

                content.add("\t\tfinal " + columnType + " " + thisAttribute
                        + " = null != this." + getter + " ? " + thisGetterTime + " : null;");

                content.add("\t\tfinal " + columnType + " " + otherAttribute
                        + " = null != other." + getter + " ? " + OtherGetterTime + " : null;");
            }  else {
                thisAttribute = "this" + Function.snakeToCamel(column.getName());
                otherAttribute = "other" + Function.snakeToCamel(column.getName());
                String getter = "get" + Function.snakeToCamel(column.getName()) + "()";

                if (Type.ENUM.equals(column.getType())) {
                    columnType = Function.snakeToCamel(column.getEnumName())
                            + configuration.getGenerest().getSuffix();
                } else if (Type.JSONB.equals(column.getType())) {
                    columnType = Type.STRING.getTypeName();
                } else if (dates.contains(column.getType())) {
                    columnType = getDateType(column.getType()).getTypeName();
                } else {
                    columnType = column.getType().getTypeName();
                }

                content.add("\t\tfinal " + columnType + " " + thisAttribute + " = this." + getter + ";");
                content.add("\t\tfinal " + columnType + " " + otherAttribute + " = other." + getter + ";");
            }

            content.add("\t\tif (!Objects.equals(" + thisAttribute + ", " + otherAttribute + ")) return false;");
        }
        content.add("\t\treturn true;");
        content.add("\t}");

        content.add("");
        content.add("\tprotected boolean canEqual(final Object other) {");
        content.add("\t\treturn other instanceof " + name + ";");
        content.add("\t}");
    }

    static void generateLombokImport(DataClassType dataClassType) {
        if (!DataClassType.DTO_POST.equals(dataClassType) || !configuration.getParameters().isDtoChangeLog()) {
            content.add("import lombok.Setter;");
        }
        content.add("import lombok.Getter;");
        content.add("import lombok.Builder;");
        content.add("import lombok.NoArgsConstructor;");
        content.add("import lombok.AllArgsConstructor;");
        content.add("import lombok.EqualsAndHashCode;");
        content.add("import lombok.ToString;");
    }

    static void generateLombokAnnotation(DataClassType dataClassType) {
        if (!DataClassType.DTO_POST.equals(dataClassType) || !configuration.getParameters().isDtoChangeLog()) {
            content.add("@Setter");
        }
        content.add("@Getter");
        content.add("@Builder");
        content.add("@NoArgsConstructor");
        content.add("@AllArgsConstructor");
        content.add("@EqualsAndHashCode");
        content.add("@ToString");
    }

    private static void generateBuilderAttribute(DataClassType dataClassType) {
        List<String> attributes = new ArrayList<>();
        for (Column column : table.getColumnList()) {
            if (!dataClassType.hasPrimaryKey() && column.isPrimary()) continue;
            if (column.getConstraintName() != null  && dataClassType.hasForeignKey()) {
                attributes.add("\t\t" + Accessor.PRIVATE.getAccess() + " " + Function.snakeToCamel(column.getFkTable()) + dataClassType.getSuffix() + configuration.getGenerest().getSuffix() + " " + Function.snakeToLowCamel(column.getFkAttribute()) + ";");
            } else if (Type.ENUM.equals(column.getType())) {
                attributes.add("\t\t" + Accessor.PRIVATE.getAccess() + " " + Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix() +" " + Function.snakeToLowCamel(column.getName()) + ";");
            } else if (Type.JSONB.equals(column.getType())) {
                attributes.add("\t\t" + Accessor.PRIVATE.getAccess() + " " + Type.STRING.getTypeName() +" " + Function.snakeToLowCamel(column.getName()) + ";");
            } else if (dates.contains(column.getType())) {
                attributes.add("\t\t" + Accessor.PRIVATE.getAccess() + " " + getDateType(column.getType()).getTypeName() + " " + Function.snakeToLowCamel(column.getName()) + ";");
            } else {
                attributes.add("\t\t" + Accessor.PRIVATE.getAccess() + " " + column.getType().getTypeName() + " " + Function.snakeToLowCamel(column.getName()) + ";");
            }
        }
        content.addAll(attributes);
    }

    private static void generateBuilderConstructor(DataClassType dataClassType) {
        content.add("");
        content.add("\t\t" + className + dataClassType.getName() + dataClassType.getSuffix() + BUILDER + "() {}");
        content.add("");
    }

    private static void generateBuilderMethods(DataClassType dataClassType) {
        List<String> methods = new ArrayList<>();
        for (Column column : table.getColumnList()) {
            if (!dataClassType.hasPrimaryKey() && column.isPrimary()) continue;
            if (column.getConstraintName() != null && dataClassType.hasForeignKey()) {
                methods.add("\t\t" + Accessor.PUBLIC.getAccess() + " " + className + dataClassType.getName() + dataClassType.getSuffix() + BUILDER + " " + Function.snakeToLowCamel(column.getFkAttribute()) + "(" + Function.snakeToCamel(column.getFkTable()) + dataClassType.getSuffix() + configuration.getGenerest().getSuffix() + " " + Function.snakeToLowCamel(column.getFkAttribute()) + ") {");
            } else if (Type.ENUM.equals(column.getType())) {
                methods.add("\t\t" + Accessor.PUBLIC.getAccess() + " " + className + dataClassType.getName() + dataClassType.getSuffix() + BUILDER + " " + Function.snakeToLowCamel(column.getName()) + "(" + Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix() + " " + Function.snakeToLowCamel(column.getName()) + ") {");
            } else if (Type.JSONB.equals(column.getType())) {
                methods.add("\t\t" + Accessor.PUBLIC.getAccess() + " " + className + dataClassType.getName() + dataClassType.getSuffix() + BUILDER + " " + Function.snakeToLowCamel(column.getName()) + "(" + Type.STRING.getTypeName() + " " + Function.snakeToLowCamel(column.getName()) + ") {");
            } else if (dates.contains(column.getType())) {
                methods.add("\t\t" + Accessor.PUBLIC.getAccess() + " " + className + dataClassType.getName() + dataClassType.getSuffix() + BUILDER + " " + Function.snakeToLowCamel(column.getName()) + "(" + getDateType(column.getType()).getTypeName() + " " + Function.snakeToLowCamel(column.getName()) + ") {");
            } else {
                methods.add("\t\t" + Accessor.PUBLIC.getAccess() + " " + className + dataClassType.getName() + dataClassType.getSuffix() + BUILDER + " " + Function.snakeToLowCamel(column.getName()) + "(" + column.getType().getTypeName() + " " + Function.snakeToLowCamel(column.getName()) + ") {");
            }

            if (column.getConstraintName() != null && dataClassType.hasForeignKey()) {
                methods.add(TAB_X_3_THIS + "." + Function.snakeToLowCamel(column.getFkAttribute()) + " = " + Function.snakeToLowCamel(column.getFkAttribute()) + ";");
            } else {
                methods.add(TAB_X_3_THIS + "." + Function.snakeToLowCamel(column.getName()) + " = " + Function.snakeToLowCamel(column.getName()) + ";");
            }

            methods.add("\t\t\treturn this;");
            methods.add(TAB_X_2 + "}");
            methods.add("");
        }

        generatedBuildMethod(dataClassType, methods);
        generateToStringMethod(dataClassType, methods, true);
        content.addAll(methods);
    }

    public static void generateToStringMethod(DataClassType dataClassType, List<String> methods, boolean builder) {
        String tab = TAB_X_1;
        if (builder) tab = TAB_X_2;

        String className = Function.snakeToCamel(table.getName()) + dataClassType.getName() + dataClassType.getSuffix() + configuration.getGenerest().getSuffix();
        if (builder) className += "." + Function.snakeToCamel(table.getName()) + dataClassType.getName() + dataClassType.getSuffix() + BUILDER;

        methods.add("");
        methods.add(tab + Accessor.PUBLIC.getAccess() + " String toString() {");
        List<String> toStringColumns = new ArrayList<>();
        for (Column column : table.getColumnList()) {
            if (!dataClassType.hasPrimaryKey() && column.isPrimary()) continue;
            if (column.getConstraintName() != null && dataClassType.hasForeignKey()) {
                toStringColumns.add(Function.snakeToLowCamel(column.getFkAttribute()) + "=\" + this." + Function.snakeToLowCamel(column.getFkAttribute()) + " + \"");
            } else {
                toStringColumns.add(Function.snakeToLowCamel(column.getName()) + "=\" + this." + Function.snakeToLowCamel(column.getName()) + " + \"");
            }
        }
        methods.add(tab + "\treturn \"" + className + "(" + String.join(", ", toStringColumns) + ")\";");
        methods.add(tab + "}");
    }

    private static void generatedBuildMethod(DataClassType dataClassType, List<String> methods) {
        methods.add(TAB_X_2 + Accessor.PUBLIC.getAccess() + " " + className + dataClassType.getName() + dataClassType.getSuffix() + configuration.getGenerest().getSuffix() + " build() {");
        List<String> buildColumns = getBuildColumns(dataClassType);
        methods.add("\t\t\treturn new " + className+ dataClassType.getName() + dataClassType.getSuffix() + configuration.getGenerest().getSuffix() + "(" + String.join(", ", buildColumns) + ");");
        methods.add(TAB_X_2 + "}");
    }

    static void generateClassEmptyConstructorMethod(List<String> constructors, DataClassType dataClassType) {
        String name = className + dataClassType.getName() + dataClassType.getSuffix() + configuration.getGenerest().getSuffix();

        constructors.add("");
        constructors.add("\t" + Accessor.PUBLIC.getAccess() + " " + name + "() {}");
    }

    static void generateClassAllArgsConstructorMethod(List<String> constructors, DataClassType dataClassType) {
        String name = className + dataClassType.getName() + dataClassType.getSuffix() + configuration.getGenerest().getSuffix();

        constructors.add("");
        String access = Accessor.PUBLIC.getAccess();
        if (dataClassType.getType().equals(ENTITY_BUILDER)) {
            access = Accessor.PRIVATE.getAccess();
        }

        constructors.add("\t" + access + " " + name + "(" + String.join(", ", getConstructColumns(dataClassType)) + ") {");
        for (Column column : table.getColumnList()) {
            if (!dataClassType.hasPrimaryKey() && column.isPrimary()) continue;
            if (column.getConstraintName() != null && dataClassType.hasForeignKey()) {
                if (dataClassType.getType().equals(ENTITY_BUILDER) && !column.isNullable()) {
                    constructors.add(TAB_X_2 + "if (" + Function.snakeToLowCamel(column.getFkAttribute()) + " == null)");
                    constructors.add(TAB_X_3 + "throw new IllegalArgumentException(\"" + name + "." + Function.snakeToLowCamel(column.getFkAttribute()) + " cannot be null\");");
                }
                constructors.add(TAB_X_2_THIS + "." + Function.snakeToLowCamel(column.getFkAttribute()) + " = " + Function.snakeToLowCamel(column.getFkAttribute()) + ";");
            } else {
                if (dataClassType.getType().equals(ENTITY_BUILDER) && !column.isNullable()) {
                    constructors.add(TAB_X_2 + "if (" + Function.snakeToLowCamel(column.getName()) + " == null)");
                    constructors.add(TAB_X_3 + "throw new IllegalArgumentException(\"" + name + "." + Function.snakeToLowCamel(column.getName()) + " cannot be null\");");
                }
                constructors.add(TAB_X_2_THIS + "." + Function.snakeToLowCamel(column.getName()) + " = " + Function.snakeToLowCamel(column.getName()) + ";");
            }
        }

        constructors.add("\t}");
    }

    static void generateClassConstructorCopyMethod(List<String> constructors, DataClassType dataClassType) {
        String name = className + dataClassType.getName() + dataClassType.getSuffix() + configuration.getGenerest().getSuffix();

        constructors.add("");
        String sourceAttribute = Function.snakeToLowCamel(table.getName()) + "Source";

        constructors.add("\t" + Accessor.PUBLIC.getAccess() + " " + name + "(" + name + " " + sourceAttribute + ") {");

        for (Column column : table.getColumnList()) {
            if (!dataClassType.hasPrimaryKey() && column.isPrimary()) continue;

            String columnAttribute = Function.snakeToLowCamel(column.getName());

            if (column.getConstraintName() != null && dataClassType.hasForeignKey()) {
                columnAttribute = Function.snakeToLowCamel(column.getFkAttribute());
            }
            constructors.add(TAB_X_2_THIS + "." + columnAttribute + " = " + sourceAttribute + ".get" + Function.snakeToCamel(columnAttribute) + "();");
        }
        constructors.add("\t}");
    }

    private static List<String> getBuildColumns(DataClassType dataClassType) {
        List<String> constructColumns = new ArrayList<>();
        for (Column column : table.getColumnList()) {
            if (!dataClassType.hasPrimaryKey() && column.isPrimary()) continue;
            if (column.getConstraintName() != null && dataClassType.hasForeignKey()) {
                constructColumns.add(Function.snakeToLowCamel(column.getFkAttribute()));
            } else {
                constructColumns.add(Function.snakeToLowCamel(column.getName()));
            }
        }
        return constructColumns;
    }

    private static List<String> getConstructColumns(DataClassType dataClassType) {
        List<String> constructColumns = new ArrayList<>();
        for (Column column : table.getColumnList()) {
            if (!dataClassType.hasPrimaryKey() && column.isPrimary()) continue;
            if (column.getConstraintName() != null && dataClassType.hasForeignKey()) {
                constructColumns.add(Function.snakeToCamel(column.getFkTable()) + dataClassType.getSuffix() + configuration.getGenerest().getSuffix() + " " + Function.snakeToLowCamel(column.getFkAttribute()));
            } else if (Type.ENUM.equals(column.getType())) {
                constructColumns.add(Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix() + " " + Function.snakeToLowCamel(column.getName()));
            } else if (Type.JSONB.equals(column.getType())) {
                constructColumns.add(Type.STRING.getTypeName() + " " + Function.snakeToLowCamel(column.getName()));
            } else if (dates.contains(column.getType())) {
                constructColumns.add(getDateType(column.getType()).getTypeName() + " " + Function.snakeToLowCamel(column.getName()));
            } else {
                constructColumns.add(column.getType().getTypeName() + " " + Function.snakeToLowCamel(column.getName()));
            }
        }
        return constructColumns;
    }
}
