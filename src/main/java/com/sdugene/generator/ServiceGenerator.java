package com.sdugene.generator;

import com.sdugene.Constant;
import com.sdugene.Function;
import com.sdugene.enums.Accessor;
import com.sdugene.enums.Type;
import com.sdugene.models.Column;
import com.sdugene.models.Configuration;
import com.sdugene.models.Table;

import java.util.ArrayList;
import java.util.List;

public class ServiceGenerator extends Generator {

    private ServiceGenerator() {
        throw new IllegalStateException("Utility class");
    }

    public static List<String> generateClass(Table table, Configuration configuration) {
        setVariables(table, configuration);
        addHeader();

        content.add(Constant.EMPTY_LINE);
        content.add("package " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.SERVICES + ";");

        content.add(Constant.EMPTY_LINE);
        generateServiceImport();
        if (configuration.getParameters().isLombok()) {
            content.add("import lombok.AllArgsConstructor;");
        }

        content.add(Constant.EMPTY_LINE);
        if (configuration.getParameters().isLombok()) {
            content.add("@AllArgsConstructor");
        }
        content.add("@Service");
        content.add(Accessor.PUBLIC.getAccess() + " class " + serviceName + " {");

        // Attributes
        List<String> attributes = new ArrayList<>();
        generateServiceAttribute(attributes);

        // Setters
        List<String> setters = new ArrayList<>();
        if (!configuration.getParameters().isLombok()) {
            generateServiceSetter(setters);
        }

        // Methods
        List<String> methods = new ArrayList<>();
        addServiceGetByIdMethod(methods);
        addServiceGetAllMethod(methods);
        addServiceGetByForeignParentMethod(methods);

        if (configuration.getParameters().isJpaSpecification()
                && configuration.getGenerated().isFilters()
                && !configuration.getExclusions().getFilters().contains(table.getName())) {
            addServiceGetByFiltersMethod(methods);
        }

        if (!table.isView()) {
            addServiceCreateMethod(methods);
            addServiceUpdateMethod(methods);
            addServiceDeleteByIdMethod(methods);
        }

        content.add(Constant.EMPTY_LINE);
        generateClassContent(attributes, setters, methods);

        content.add("}");
        return content;
    }

    private static void addServiceGetByIdMethod(List<String> method) {
        method.add(Constant.EMPTY_LINE);
        method.add(Constant.TAB_AT_TRANSACTIONAL + "(readOnly = true)");
        method.add(Constant.TAB_X_1 + Accessor.PUBLIC.getAccess() + " Optional<" + entityName + "> get(" + table.getColumnList().get(0).getType().getTypeName() + " " + table.getColumnList().get(0).getName() + ") {");
        method.add(Constant.TAB_X_2 + Constant.RETURN + " " + repositoryAttribute + ".findById(" + table.getColumnList().get(0).getName() + ");");
        method.add(Constant.TAB_X_1 + "}");
    }

    private static void addServiceGetAllMethod(List<String> method) {
        method.add(Constant.EMPTY_LINE);
        method.add(Constant.TAB_AT_TRANSACTIONAL + "(readOnly = true)");
        method.add(Constant.TAB_X_1 + Accessor.PUBLIC.getAccess() + " List<" + entityName + "> getAll() {");
        method.add(Constant.TAB_X_2 + Constant.RETURN + " " + repositoryAttribute + ".findAll();");
        method.add(Constant.TAB_X_1 + "}");
    }

    private static void addServiceGetByForeignParentMethod(List<String> method) {
        for (Column column : table.getColumnList()) {
            if (column.getConstraintName() != null) {
                String tableIdName = Function.snakeToCamel(column.getName());
                String tableIdAttribute = Function.snakeToLowCamel(column.getName());
                Type tableIdType = column.getType();

                method.add(Constant.EMPTY_LINE);
                method.add("\t@Transactional(readOnly = true)");
                if (column.isUnique()) {
                    method.add(Constant.TAB_X_1 + Accessor.PUBLIC.getAccess() + " Optional<" + entityName + "> getBy" + tableIdName + "(" + tableIdType.getTypeName() + " " + tableIdAttribute + ") {");
                    method.add(Constant.TAB_X_2 + Constant.RETURN + " " + repositoryAttribute + ".findFirstBy" + tableIdName + "(" + tableIdAttribute + ");");
                } else {
                    method.add(Constant.TAB_X_1 + Accessor.PUBLIC.getAccess() + " List<" + entityName + "> getBy" + tableIdName + "(" + tableIdType.getTypeName() + " " + tableIdAttribute + ") {");
                    method.add(Constant.TAB_X_2 + Constant.RETURN + " " + repositoryAttribute + ".findBy" + tableIdName + "(" + tableIdAttribute + ");");
                }
                method.add(Constant.TAB_X_1 + "}");
            }
        }
    }

    private static void addServiceGetByFiltersMethod(List<String> method) {
        method.add(Constant.EMPTY_LINE);
        method.add("\t@Transactional(readOnly = true)");

        method.add(Constant.TAB_X_1 + Accessor.PUBLIC.getAccess() + " List<" + entityName + "> getByFilters(" + filterName + " " + filterAttribute + ") {");
        method.add(Constant.TAB_X_2 + "Specification<" + entityName + "> specification = Specification.where(has" +  Function.snakeToCamel(table.getColumnList().get(0).getName()) + "());");
        method.add(Constant.TAB_X_2 + "return this.getByFilters(" + filterAttribute + ", specification);");
        method.add(Constant.TAB_X_1 + "}");

        method.add(Constant.EMPTY_LINE);
        method.add(Constant.TAB_X_1 + Accessor.PUBLIC.getAccess() + " List<" + entityName + "> getByFilters(" + filterName + " " + filterAttribute + ", Specification<" + entityName + "> specification) {");

        for (Column column : table.getColumnList()) {
            method.add(Constant.TAB_X_2 + "if (" + filterAttribute +".get" + Function.snakeToCamel(column.getName()) + "() != null) {");
            method.add(Constant.TAB_X_3 + "specification = specification.and(has" + Function.snakeToCamel(column.getName()) + "(" + filterAttribute +".get" + Function.snakeToCamel(column.getName()) + "()));");
            method.add(Constant.TAB_X_2 + "}");
        }

        method.add(Constant.EMPTY_LINE);
        method.add(Constant.TAB_X_2 + "return " + repositoryAttribute + ".findAll(specification);");

        method.add(Constant.TAB_X_1 + "}");
    }

    private static void addServiceCreateMethod(List<String> method) {
        method.add(Constant.EMPTY_LINE);
        method.add(Constant.TAB_AT_TRANSACTIONAL);
        method.add(Constant.TAB_X_1 + Accessor.PUBLIC.getAccess() + " " + entityName + " create("+ entityName +" "+ entityAttribute +") {");

        final Column primaryColumn = table.getPrimaryColumn();
        if (null != primaryColumn && Type.UUID.equals(primaryColumn.getType())) {
            final String primaryKeyName = Function.snakeToCamel(primaryColumn.getName());
            method.add(Constant.TAB_X_2 + "if (null == " + entityAttribute + ".get" + primaryKeyName + "()){");
            method.add(Constant.TAB_X_3 + entityAttribute + ".set" + primaryKeyName + "(UUID.randomUUID());");
            method.add(Constant.TAB_X_2 + "}");
        }
        methodConstraintValidation(method);
        method.add(Constant.TAB_X_2 + Constant.RETURN + " " + repositoryAttribute + ".save(" + entityAttribute + ");");
        method.add(Constant.TAB_X_1 + "}");
    }

    private static void addServiceUpdateMethod(List<String> method) {
        method.add(Constant.EMPTY_LINE);
        method.add(Constant.TAB_AT_TRANSACTIONAL);
        method.add(Constant.TAB_X_1 + Accessor.PUBLIC.getAccess() + " " + entityName + " update("+ entityName +" "+ entityAttribute +") {");
        methodConstraintValidation(method);
        method.add(Constant.TAB_X_2 + Constant.RETURN + " " + repositoryAttribute + ".save(" + entityAttribute + ");");
        method.add(Constant.TAB_X_1 + "}");
    }

    private static void addServiceDeleteByIdMethod(List<String> method) {
        method.add(Constant.EMPTY_LINE);
        method.add(Constant.TAB_AT_TRANSACTIONAL);
        method.add(Constant.TAB_X_1 + Accessor.PUBLIC.getAccess() + " boolean deleteById(" + table.getColumnList().get(0).getType().getTypeName() + " " + table.getColumnList().get(0).getName() + ") {");
        method.add(Constant.TAB_X_2 + "if (" + repositoryAttribute + ".existsById(" + table.getColumnList().get(0).getName() + ")) {");
        method.add(Constant.TAB_X_3 + repositoryAttribute + ".deleteById(" + table.getColumnList().get(0).getName() + ");");
        method.add(Constant.TAB_X_3 + Constant.RETURN + " true;");
        method.add(Constant.TAB_X_2 + "} else {");
        method.add(Constant.TAB_X_3 + Constant.RETURN + " false;");
        method.add(Constant.TAB_X_2 + "}");
        method.add(Constant.TAB_X_1 + "}");
    }

    private static void methodConstraintValidation(List<String> method) {
        for (Column column : table.getColumnList()) {
            if (column.getConstraintName() != null) {
                String tableServiceAttribute = Function.snakeToLowCamel(column.getFkTable()) + Constant.SERVICE + configuration.getGenerest().getSuffix();

                if (Boolean.TRUE.equals(column.isNullable())) {
                    method.add(Constant.TAB_X_2 + "if (" + entityAttribute + ".get" + Function.snakeToCamel(column.getFkAttribute()) + "() != null) {");
                    method.add(Constant.TAB_X_3 + entityAttribute + ".set"
                            + Function.snakeToCamel(column.getFkAttribute())
                            + "(" + tableServiceAttribute + ".get("
                            + entityAttribute + ".get" + Function.snakeToCamel(column.getFkAttribute()) + "().get" + Function.snakeToCamel(column.getFkColumn()) + "()"
                            + ").orElse(null));");
                    method.add(Constant.TAB_X_2 + "}");
                } else {
                    method.add(Constant.TAB_X_2 + entityAttribute + ".set"
                            + Function.snakeToCamel(column.getFkAttribute())
                            + "(" + tableServiceAttribute + ".get("
                            + entityAttribute + ".get" + Function.snakeToCamel(column.getFkAttribute()) + "().get" + Function.snakeToCamel(column.getFkColumn()) + "()"
                            + ").orElse(null));");
                }
            }
        }
    }

    private static void generateServiceAttribute(List<String> attributes) {
        List<String> attributePresent = new ArrayList<>();
        String finalStr = configuration.getParameters().isLombok() ? " final " : " ";

        attributes.add(Constant.TAB_X_1 + Accessor.PRIVATE.getAccess() + finalStr + repositoryName + " " + repositoryAttribute + ";");

        for (Column column : table.getColumnList()) {
            if (column.getConstraintName() != null && !attributePresent.contains(column.getFkTable())) {
                String tableServiceName = Function.snakeToCamel(column.getFkTable()) + Constant.SERVICE + configuration.getGenerest().getSuffix();
                String tableServiceAttribute = Function.snakeToLowCamel(column.getFkTable()) + Constant.SERVICE + configuration.getGenerest().getSuffix();
                attributes.add(Constant.TAB_X_1 + Accessor.PRIVATE.getAccess() + finalStr + tableServiceName + " " + tableServiceAttribute + ";");
                attributePresent.add(column.getFkTable());
            }
        }
    }

    private static void generateServiceSetter(List<String> setters) {
        List<String> setterPresent = new ArrayList<>();
        generateAutowiredSetter(repositoryName, repositoryAttribute, serviceName, setters);

        for (Column column : table.getColumnList()) {
            if (column.getConstraintName() != null && !setterPresent.contains(column.getFkTable())) {
                String tableServiceName = Function.snakeToCamel(column.getFkTable()) + Constant.SERVICE + configuration.getGenerest().getSuffix();
                String tableServiceAttribute = Function.snakeToLowCamel(column.getFkTable()) + Constant.SERVICE + configuration.getGenerest().getSuffix();
                generateAutowiredSetter(tableServiceName, tableServiceAttribute, serviceName, setters);
                setterPresent.add(column.getFkTable());
            }
        }
    }

    private static void generateServiceImport() {
        content.add(importString + Constant.ENTITIES + "." + entityName + ";");

        content.add(importString + Constant.REPOSITORIES + "." + Constant.RELATIONAL + "." + repositoryName + ";");

        if (configuration.getParameters().isJpaSpecification()
                && configuration.getGenerated().isFilters()
                && !configuration.getExclusions().getFilters().contains(table.getName())) {
            content.add("import org.springframework.data.jpa.domain.Specification;");
            content.add(importString + Constant.ENTITIES + "." + Constant.FILTERS + "." + filterName + ";");
            content.add(importStaticString + Constant.REPOSITORIES + "." + Constant.SPECIFICATIONS + "." + repositorySpecificationsName + ".*;");

        }

        if (!configuration.getParameters().isLombok()) {
            content.add("import org.springframework.beans.factory.annotation.Autowired;");
        }

        content.add("import org.springframework.transaction.annotation.Transactional;");
        content.add("import org.springframework.stereotype.Service;");

        content.add(Constant.EMPTY_LINE);
        content.add("import java.util.Optional;");
        content.add("import java.util.List;");
        generateUUIDImport();
    }
}
