package com.sdugene.generator;

import com.sdugene.Constant;
import com.sdugene.models.Configuration;
import java.util.List;

public class ConfigurationGenerator extends Generator {

    private ConfigurationGenerator() {
        throw new IllegalStateException("Utility class");
    }

    public static List<String> generateSwaggerConfig(Configuration configuration) {
        setVariables(null, configuration);
        addHeader();

        content.add("");
        content.add("package " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.CONFIGURATIONS + ";");

        content.add("");
        content.add("import org.springframework.context.annotation.Bean;");
        content.add("import org.springframework.context.annotation.Configuration;");
        content.add("import org.springframework.web.bind.annotation.RestController;");
        content.add("import springfox.documentation.builders.PathSelectors;");
        content.add("import springfox.documentation.builders.RequestHandlerSelectors;");
        content.add("import springfox.documentation.spi.DocumentationType;");
        content.add("import springfox.documentation.spring.web.plugins.Docket;");
        content.add("import springfox.documentation.swagger2.annotations.EnableSwagger2;");

        content.add("");
        content.add("@Configuration");
        content.add("@EnableSwagger2");
        content.add("public class Swagger" + Constant.CONFIG + configuration.getGenerest().getSuffix() + " {");

        content.add("");
        content.add("\t@Bean");
        content.add("\tpublic Docket api() {");
        content.add("\t\treturn new Docket(DocumentationType.SWAGGER_2)");
        content.add("\t\t\t.select()");
        content.add("\t\t\t.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))");
        content.add("\t\t\t.paths(PathSelectors.any())");
        content.add("\t\t\t.build();");
        content.add("\t}");

        content.add("}");
        return content;
    }
}
