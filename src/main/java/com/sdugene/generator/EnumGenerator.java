package com.sdugene.generator;

import com.sdugene.Constant;
import com.sdugene.Function;
import com.sdugene.enums.Accessor;
import com.sdugene.models.Column;
import com.sdugene.models.Configuration;
import com.sdugene.models.EnumType;
import com.sdugene.models.Table;

import java.util.ArrayList;
import java.util.List;

public class EnumGenerator extends Generator {

    private EnumGenerator() {
        throw new IllegalStateException("Utility class");
    }

    public static List<String> generateAttributeInterface(Configuration configuration) {
        setVariables(table, configuration);
        addHeader();
        addPackage();

        content.add("");
        content.add(Accessor.PUBLIC.getAccess() + " interface IAttribute" + configuration.getGenerest().getSuffix() + " {");
        content.add("}");
        return content;
    }

    public static List<String> generateEnum(List<Table> tables, Configuration configuration) {
        content = new ArrayList<>();
        addHeader();
        addPackage();

        content.add("");
        content.add(Accessor.PUBLIC.getAccess() + " enum Attribute" +
                configuration.getGenerest().getSuffix() + " implements IAttribute" + configuration.getGenerest().getSuffix() + " {");

        List<String> enumerations = generateEnumeration(tables);

        for (int key = 0 ; key < enumerations.size() ; key++) {
            if (key+1 == enumerations.size()) {
                content.add("\t" + enumerations.get(key));
            } else {
                content.add("\t" + enumerations.get(key) + ",");
            }
        }

        content.add("}");
        return content;
    }

    public static List<String> generateEnum(EnumType enumType, Configuration configuration) {
        content = new ArrayList<>();
        addHeader();
        addPackage();

        content.add("");
        content.add(Accessor.PUBLIC.getAccess() + " enum " + Function.snakeToCamel(enumType.getName()) + configuration.getGenerest().getSuffix() + " {");

        for (int key = 0 ; key < enumType.getValueList().size() ; key++) {
            if (key+1 == enumType.getValueList().size()) {
                content.add("\t" + enumType.getValueList().get(key));
            } else {
                content.add("\t" + enumType.getValueList().get(key) + ",");
            }
        }

        content.add("}");
        return content;
    }

    public static List<String> generatePostgreSQLEnumType() {
        content = new ArrayList<>();
        addHeader();
        addPackage();

        content.add("");
        content.add("import org.hibernate.engine.spi.SharedSessionContractImplementor;");

        content.add("");
        content.add("import java.sql.PreparedStatement;");
        content.add("import java.sql.SQLException;");
        content.add("import org.hibernate.type.EnumType;");
        content.add("import java.sql.Types;");

        content.add("");
        content.add("public class PostgreSQLEnumType extends EnumType {");

        content.add("");
        content.add("\t@Override");
        content.add("\tpublic void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws SQLException {");
        content.add("\t\tif (value == null) {");
        content.add("\t\t\tst.setNull( index, Types.OTHER );");

        content.add("\t\t} else {");
        content.add("\t\t\tst.setObject(index, value.toString(), Types.OTHER);");

        content.add("\t\t}");
        content.add("\t}");
        content.add("}");
        return content;
    }

    private static void addPackage() {
        content.add("");
        content.add("package "
                + configuration.getJava().getDefaultPackageDotted()
                + configuration.getGenerest().getPackageNameDoted()
                + Constant.ENUMS + ";");
    }

    private static List<String> generateEnumeration(List<Table> tables) {
        List<String> enumerations = new ArrayList<>();
        List<String> attributes = new ArrayList<>();

        for(Table table : tables) {
            for (Column column : table.getColumnList()) {
                String name;
                if (column.getConstraintName() != null) {
                    name = column.getFkAttribute().toUpperCase();
                    if (!attributes.contains(name)) {
                        attributes.add(name);
                        enumerations.add(name);
                    }
                }

                name = column.getName().toUpperCase();
                if (!attributes.contains(name)) {
                    attributes.add(name);
                    enumerations.add(name);
                }
            }
        }

        return enumerations;
    }
}
