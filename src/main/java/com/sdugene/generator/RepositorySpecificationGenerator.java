package com.sdugene.generator;

import com.sdugene.Constant;
import com.sdugene.Function;
import com.sdugene.enums.Accessor;
import com.sdugene.enums.Type;
import com.sdugene.models.Column;
import com.sdugene.models.Configuration;
import com.sdugene.models.Table;

import java.util.ArrayList;
import java.util.List;

public class RepositorySpecificationGenerator extends Generator {

    private RepositorySpecificationGenerator() {
        throw new IllegalStateException("Utility class");
    }

    public static List<String> generateClass(Table table, Configuration configuration) {
        setVariables(table, configuration);
        addHeader();

        content.add(Constant.EMPTY_LINE);
        content.add("package " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.REPOSITORIES + "." + Constant.SPECIFICATIONS + ";");

        content.add(Constant.EMPTY_LINE);
        generateRepositorySpecificationImport();

        content.add(Constant.EMPTY_LINE);
        content.add(Accessor.PUBLIC.getAccess() + " class " + repositorySpecificationsName + " {");

        // Constructor
        List<String> constructors = new ArrayList<>();
        constructors.add("\t" + Accessor.PRIVATE.getAccess() + " " + repositorySpecificationsName + "() {");
        constructors.add("\t\tthrow new IllegalStateException(\"Utility class\");");
        constructors.add("\t}");

        // Methods
        List<String> methods = new ArrayList<>();
        for (Column column : table.getColumnList()) {
            addHasColumnNotNullMethod(column, methods);
            addHasColumnNullMethod(column, methods);
            addHasColumnMethod(column, methods);
            addHasNotColumnMethod(column, methods);
        }

        content.add(Constant.EMPTY_LINE);
        generateClassContent(constructors, methods);

        content.add("}");
        return content;
    }

    private static void addHasColumnNotNullMethod(Column column, List<String> methods) {
        methods.add(Constant.EMPTY_LINE);
        methods.add("\t" + Accessor.PUBLIC.getAccess() + " static Specification<" + entityName + "> has" +  Function.snakeToCamel(column.getName()) + "() {");

        if (column.getConstraintName() != null) {
            methods.add("\t\treturn (" + entityAttribute + ", cq, cb) -> cb.isNotNull(" + entityAttribute + ".get(\""+ Function.snakeToLowCamel(column.getFkAttribute()) + "\").get(\""+ Function.snakeToLowCamel(column.getFkColumn()) + "\"));");
        } else {
            methods.add("\t\treturn (" + entityAttribute + ", cq, cb) -> cb.isNotNull(" + entityAttribute + ".get(\"" + Function.snakeToLowCamel(column.getName()) + "\"));");
        }

        methods.add("\t}");
    }

    private static void addHasColumnNullMethod(Column column, List<String> methods) {
        methods.add(Constant.EMPTY_LINE);
        methods.add("\t" + Accessor.PUBLIC.getAccess() + " static Specification<" + entityName + "> hasNot" +  Function.snakeToCamel(column.getName()) + "() {");

        if (column.getConstraintName() != null) {
            methods.add("\t\treturn (" + entityAttribute + ", cq, cb) -> cb.isNull(" + entityAttribute + ".get(\""+ Function.snakeToLowCamel(column.getFkAttribute()) + "\").get(\""+ Function.snakeToLowCamel(column.getFkColumn()) + "\"));");
        } else {
            methods.add("\t\treturn (" + entityAttribute + ", cq, cb) -> cb.isNull(" + entityAttribute + ".get(\"" + Function.snakeToLowCamel(column.getName()) + "\"));");
        }

        methods.add("\t}");
    }

    private static void addHasColumnMethod(Column column, List<String> methods) {
        methods.add(Constant.EMPTY_LINE);

        if (Type.ENUM.equals(column.getType())) {
            methods.add("\t" + Accessor.PUBLIC.getAccess() + " static Specification<" + entityName + "> has" +  Function.snakeToCamel(column.getName()) + "(" + Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix() +  " " + Function.snakeToLowCamel(column.getName()) + ") {");
        } else if (dates.contains(column.getType())) {
            methods.add("\t" + Accessor.PUBLIC.getAccess() + " static Specification<" + entityName + "> has" +  Function.snakeToCamel(column.getName()) + "(" + getDateType(column.getType()).getTypeName() +  " " + Function.snakeToLowCamel(column.getName()) + ") {");
        } else {
            methods.add("\t" + Accessor.PUBLIC.getAccess() + " static Specification<" + entityName + "> has" +  Function.snakeToCamel(column.getName()) + "(" + column.getType().getTypeName() + " " + Function.snakeToLowCamel(column.getName()) + ") {");
        }

        if (Type.STRING.equals(column.getType())) {
            methods.add("\t\treturn (" + entityAttribute + ", cq, cb) -> cb.like(cb.lower(" + entityAttribute + ".get(\"" + Function.snakeToLowCamel(column.getName()) + "\")), \"%\" + " + Function.snakeToLowCamel(column.getName()) + ".toLowerCase() + \"%\");");
        } else if (column.getConstraintName() != null) {
            methods.add("\t\treturn (" + entityAttribute + ", cq, cb) -> cb.equal(" + entityAttribute + ".get(\""+ Function.snakeToLowCamel(column.getFkAttribute()) + "\").get(\""+ Function.snakeToLowCamel(column.getFkColumn()) + "\"), " + Function.snakeToLowCamel(column.getName()) + ");");
        } else {
            methods.add("\t\treturn (" + entityAttribute + ", cq, cb) -> cb.equal(" + entityAttribute + ".get(\""+ Function.snakeToLowCamel(column.getName()) + "\"), " + Function.snakeToLowCamel(column.getName()) + ");");
        }

        methods.add("\t}");
    }

    private static void addHasNotColumnMethod(Column column, List<String> methods) {
        methods.add(Constant.EMPTY_LINE);

        if (Type.ENUM.equals(column.getType())) {
            methods.add("\t" + Accessor.PUBLIC.getAccess() + " static Specification<" + entityName + "> hasNot" +  Function.snakeToCamel(column.getName()) + "(" + Function.snakeToCamel(column.getEnumName()) + configuration.getGenerest().getSuffix() +  " " + Function.snakeToLowCamel(column.getName()) + ") {");
        } else if (dates.contains(column.getType())) {
            methods.add("\t" + Accessor.PUBLIC.getAccess() + " static Specification<" + entityName + "> hasNot" +  Function.snakeToCamel(column.getName()) + "(" + getDateType(column.getType()).getTypeName() +  " " + Function.snakeToLowCamel(column.getName()) + ") {");
        } else {
            methods.add("\t" + Accessor.PUBLIC.getAccess() + " static Specification<" + entityName + "> hasNot" +  Function.snakeToCamel(column.getName()) + "(" + column.getType().getTypeName() + " " + Function.snakeToLowCamel(column.getName()) + ") {");
        }

        if (Type.STRING.equals(column.getType())) {
            methods.add("\t\treturn (" + entityAttribute + ", cq, cb) -> cb.notLike(cb.lower(" + entityAttribute + ".get(\"" + Function.snakeToLowCamel(column.getName()) + "\")), \"%\" + " + Function.snakeToLowCamel(column.getName()) + ".toLowerCase() + \"%\");");
        } else if (column.getConstraintName() != null) {
            methods.add("\t\treturn (" + entityAttribute + ", cq, cb) -> cb.notEqual(" + entityAttribute + ".get(\""+ Function.snakeToLowCamel(column.getFkAttribute()) + "\").get(\""+ Function.snakeToLowCamel(column.getFkColumn()) + "\"), " + Function.snakeToLowCamel(column.getName()) + ");");
        } else {
            methods.add("\t\treturn (" + entityAttribute + ", cq, cb) -> cb.notEqual(" + entityAttribute + ".get(\""+ Function.snakeToLowCamel(column.getName()) + "\"), " + Function.snakeToLowCamel(column.getName()) + ");");
        }

        methods.add("\t}");
    }

    private static void generateRepositorySpecificationImport() {
        List<String> importPresent = new ArrayList<>();

        for (Column column : table.getColumnList()) {
            generateTypeImport(column, importPresent, false);
            generateEnumImport(column, importPresent);
        }
        content.add(importString + Constant.ENTITIES + "." + entityName + ";");
        content.add("import org.springframework.data.jpa.domain.Specification;");
    }
}
