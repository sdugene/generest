package com.sdugene.generator;

import com.sdugene.Constant;
import com.sdugene.Function;
import com.sdugene.enums.Accessor;
import com.sdugene.enums.Type;
import com.sdugene.models.Column;
import com.sdugene.models.Configuration;
import com.sdugene.models.Table;

import java.util.ArrayList;
import java.util.List;

public class RepositoryGenerator extends Generator {

    private RepositoryGenerator() {
        throw new IllegalStateException("Utility class");
    }

    public static List<String> generateClass(Table table, Configuration configuration) {
        setVariables(table, configuration);
        addHeader();

        content.add("");
        content.add("package " + configuration.getJava().getDefaultPackageDotted() + configuration.getGenerest().getPackageNameDoted() + Constant.REPOSITORIES + "." + Constant.RELATIONAL + ";");

        content.add("");
        content.add(importString + Constant.ENTITIES + "." + entityName + ";");
        content.add("import org.springframework.data.jpa.repository.JpaRepository;");
        if (configuration.getParameters().isJpaSpecification()) {
            content.add("import org.springframework.data.jpa.repository.JpaSpecificationExecutor;");
        }
        content.add("import org.springframework.stereotype." + Constant.REPOSITORY + ";");

        content.add("");
        generateRepositoryImport();

        content.add("");
        content.add("@" + Constant.REPOSITORY);

        Type primaryType = table.getColumnList()
                .stream()
                .filter(column -> column.isPrimary(table.getColumnList()))
                .findFirst()
                .map(Column::getType)
                .orElse(table.getColumnList().get(0).getType());

        if (configuration.getParameters().isJpaSpecification()) {
            content.add(Accessor.PUBLIC.getAccess()
                    + " interface " + repositoryName + " extends JpaRepository<" + entityName
                    + ", " + primaryType.getTypeName() + ">, JpaSpecificationExecutor {");
        } else {
            content.add(Accessor.PUBLIC.getAccess()
                    + " interface " + repositoryName + " extends JpaRepository<" + entityName
                    + ", " + primaryType.getTypeName() + "> {");
        }

        for(Column column : table.getColumnList()) {
            if (column.getConstraintName() != null) {
                String tableIdName = Function.snakeToCamel(column.getName());
                String tableIdAttribute = Function.snakeToLowCamel(column.getName());
                Type tableIdType = column.getType();

                content.add("");
                if (column.isUnique()) {
                    content.add("\t" + " Optional<" + entityName + "> findFirstBy" + tableIdName + "(" + tableIdType.getTypeName() + " " + tableIdAttribute + ");");
                } else {
                    content.add("\t" + " List<" + entityName + "> findBy" + tableIdName + "(" + tableIdType.getTypeName() + " " + tableIdAttribute + ");");
                }
            }

        }

        content.add("}");
        return content;
    }

    private static void generateRepositoryImport() {
        List<String> importPresent = new ArrayList<>();

        generateUUIDImport();
        for(Column column : table.getColumnList()) {
            if (column.getConstraintName() != null
                    && !column.isUnique()
                    && !importPresent.contains("List")) {
                content.add("import java.util.List;");
                importPresent.add("List");
            }

            if (column.getConstraintName() != null
                    && column.isUnique()
                    && !importPresent.contains("Optional")) {
                content.add("import java.util.Optional;");
                importPresent.add("Optional");
            }
        }
    }
}
