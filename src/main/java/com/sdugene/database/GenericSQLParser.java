package com.sdugene.database;

import com.sdugene.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

class GenericSQLParser implements SQLParser {

    public List<Table> getTables(List<EnumType> enumTypeList) {
       return new ArrayList<>();
    }

    public List<View> getViews(List<EnumType> enumTypeList) {
        return new ArrayList<>();
    }

    public List<MaterializedView> getMaterializedViews(List<EnumType> enumTypeList) {
        return new ArrayList<>();
    }

    public List<EnumType> getEnumTypes() {
        return new ArrayList<>();
    }

    Table createTable(
          String tableName,
          List<EnumType> enumTypeList,
          List<Map<String, String>> columnList
    ) {
        return this.createTable(tableName, enumTypeList, columnList, new ArrayList<>());
    }

    Table createTable(
            String tableName,
            List<EnumType> enumTypeList,
            List<Map<String, String>> columnList,
            List<Map<String, String>> joinList
    ) {
        Table table = new Table(tableName);

        IntStream.range(0, columnList.size()).forEach(index -> table
                .getColumnList()
                .add(new Column(columnList.get(index), enumTypeList, "column", index)));

        IntStream.range(0, joinList.size()).forEach(index -> table
                .getJoinList()
                .add(new Column(joinList.get(index), enumTypeList, "joinColumn", index)));

        return table;
    }

    List<String> setColumnNames() {
        List<String> columnNames = new ArrayList<>();
        columnNames.add("column_name");
        columnNames.add("is_nullable");
        columnNames.add("is_primary");
        columnNames.add("is_unique");
        columnNames.add("type");
        columnNames.add("length");
        columnNames.add("constraint_name");
        columnNames.add("foreign_column_name");
        columnNames.add("foreign_table_name");

        return columnNames;
    }
}
