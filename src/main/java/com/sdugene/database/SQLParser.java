package com.sdugene.database;

import com.sdugene.models.EnumType;
import com.sdugene.models.MaterializedView;
import com.sdugene.models.Table;
import com.sdugene.models.View;

import java.util.List;

public interface SQLParser {

    List<Table> getTables(List<EnumType> enumTypeList);

    List<View> getViews(List<EnumType> enumTypeList);

    List<MaterializedView> getMaterializedViews(List<EnumType> enumTypeList);

    List<EnumType> getEnumTypes();
}