package com.sdugene.database;

import com.sdugene.models.EnumType;
import com.sdugene.models.Table;
import com.sdugene.models.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MySQLParser extends GenericSQLParser {

    private Accessor accessor;

    MySQLParser(Accessor accessor) {
        this.accessor = accessor;
    }

    @Override
    public List<Table> getTables(List<EnumType> enumTypeList) {
        String attribute = "table";
        List<Table> tables = new ArrayList<>();

        List<String> tableList = accessor
                .executeQuery("SELECT table_name as '" + attribute
                        + "' FROM information_schema.tables where table_schema = '"
                        + accessor.configuration.getDatasource().getSchema()
                        + "'", attribute);

        for(String tableName : tableList) {
            tables.add(createTable(tableName, enumTypeList, getColumns(tableName), getJoins(tableName)));
        }
        return tables;
    }

    // TODO getViews method
    @Override
    public List<View> getViews(List<EnumType> enumTypeList) {
        return new ArrayList<>();
    }

    private List<Map<String, String>> getColumns(String tableName) {
        String queryPart1 = "SELECT" +
                " kcu.COLUMN_NAME," +
                " MAX(CASE WHEN tc.CONSTRAINT_TYPE = 'PRIMARY KEY' THEN tc.CONSTRAINT_TYPE END) 'PRIMARY'," +
                " MAX(CASE WHEN tc.CONSTRAINT_TYPE = 'unique' THEN tc.CONSTRAINT_TYPE END) 'UNIQUE'" +
                " FROM information_schema.TABLE_CONSTRAINTS tc" +
                " JOIN information_schema.KEY_COLUMN_USAGE kcu on tc.TABLE_SCHEMA = kcu.TABLE_SCHEMA and tc.TABLE_NAME = kcu.TABLE_NAME" +
                " AND tc.CONSTRAINT_NAME = kcu.CONSTRAINT_NAME" +
                " WHERE tc.TABLE_SCHEMA = '" + accessor.configuration.getDatasource().getSchema() + "'" +
                " AND tc.TABLE_NAME = '" + tableName + "'" +
                " AND tc.CONSTRAINT_TYPE in ('PRIMARY KEY', 'UNIQUE')" +
                " GROUP BY kcu.COLUMN_NAME";

        String queryPart2 = "SELECT" +
                " c.COLUMN_NAME as 'column_name'," +
                " c.IS_NULLABLE as 'is_nullable'," +
                " c.DATA_TYPE as 'type'," +
                " c.CHARACTER_MAXIMUM_LENGTH as 'length'," +
                " IF(ct.`PRIMARY` = 'PRIMARY KEY', 'true', 'false') as is_primary," +
                " IF(ct.`UNIQUE` = 'UNIQUE', 'true', 'false') as is_unique," +
                " kcu.CONSTRAINT_NAME as 'constraint_name'," +
                " kcu.REFERENCED_COLUMN_NAME as 'foreign_column_name'," +
                " kcu.REFERENCED_TABLE_NAME as 'foreign_table_name'" +
                " FROM information_schema.COLUMNS c" +
                " LEFT JOIN information_schema.KEY_COLUMN_USAGE kcu on c.TABLE_SCHEMA = kcu.TABLE_SCHEMA and c.TABLE_NAME = kcu.TABLE_NAME" +
                " and c.COLUMN_NAME = kcu.COLUMN_NAME and kcu.REFERENCED_COLUMN_NAME IS NOT NULL" +
                " LEFT JOIN constraint_type ct on c.COLUMN_NAME = ct.COLUMN_NAME" +
                " WHERE c.TABLE_SCHEMA = '" + accessor.configuration.getDatasource().getSchema() + "'" +
                " AND c.TABLE_NAME = '" + tableName + "'" +
                " group by c.COLUMN_NAME" +
                " order by c.ORDINAL_POSITION";

        String query = "with constraint_type AS (" + queryPart1 + ") " + queryPart2;

        return accessor
                .executeQuery(query, setColumnNames());
    }

    private List<Map<String, String>> getJoins(String tableName) {
        String query = "SELECT" +
                " c.COLUMN_NAME as 'column_name'," +
                " c.IS_NULLABLE as 'is_nullable'," +
                " c.DATA_TYPE as 'type'," +
                " c.CHARACTER_MAXIMUM_LENGTH as 'length'," +
                " 'false' as is_primary," +
                " IF(tc.CONSTRAINT_TYPE = 'UNIQUE', 'true', 'false') as is_unique," +
                " kcu.CONSTRAINT_NAME as 'constraint_name'," +
                " kcu.COLUMN_NAME as 'foreign_column_name'," +
                " kcu.TABLE_NAME as 'foreign_table_name'" +
                " FROM INFORMATION_SCHEMA.COLUMNS c" +
                " INNER JOIN information_schema.KEY_COLUMN_USAGE kcu on c.TABLE_SCHEMA = kcu.TABLE_SCHEMA" +
                " and c.COLUMN_NAME = kcu.REFERENCED_COLUMN_NAME and c.TABLE_NAME = kcu.REFERENCED_TABLE_NAME" +
                " LEFT JOIN information_schema.KEY_COLUMN_USAGE kcut on c.TABLE_SCHEMA = kcut.TABLE_SCHEMA" +
                " and kcu.COLUMN_NAME = kcut.COLUMN_NAME and kcu.TABLE_NAME = kcut.TABLE_NAME" +
                " LEFT JOIN information_schema.TABLE_CONSTRAINTS tc on c.TABLE_SCHEMA = tc.TABLE_SCHEMA" +
                " and kcu.TABLE_NAME = tc.TABLE_NAME and tc.CONSTRAINT_NAME = kcut.CONSTRAINT_NAME and CONSTRAINT_TYPE = 'UNIQUE'" +
                " WHERE c.TABLE_SCHEMA = '" + accessor.configuration.getDatasource().getSchema() +
                "' AND c.TABLE_NAME = '" + tableName + "'" +
                " group by c.COLUMN_NAME" +
                " order by c.ORDINAL_POSITION";

        return accessor
                .executeQuery(query, setColumnNames());
    }
}