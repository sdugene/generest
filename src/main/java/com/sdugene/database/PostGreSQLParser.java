package com.sdugene.database;

import com.sdugene.models.EnumType;
import com.sdugene.models.MaterializedView;
import com.sdugene.models.Table;
import com.sdugene.models.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PostGreSQLParser extends GenericSQLParser {

    private final Accessor accessor;

    PostGreSQLParser(Accessor accessor) {
        this.accessor = accessor;
    }

    @Override
    public List<Table> getTables(List<EnumType> enumTypeList) {
        String attribute = "table";
        List<Table> tables = new ArrayList<>();

        List<String> tableList = accessor
                .executeQuery("select tablename as " + attribute
                        + " from pg_tables where schemaname = '"
                        + accessor.configuration.getDatasource().getSchema()
                        + "'", attribute);

        for(String tableName : tableList) {
            tables.add(createTable(tableName, enumTypeList, getColumns(tableName), getJoins(tableName)));
        }
        return tables;
    }

    @Override
    public List<View> getViews(List<EnumType> enumTypeList) {
        String attribute = "view";
        List<View> views = new ArrayList<>();

        List<String> viewList = accessor
                .executeQuery("select viewname as " + attribute
                        + " from pg_views where schemaname = '"
                        + accessor.configuration.getDatasource().getSchema()
                        + "'", attribute);

        for(String viewName : viewList) {
            views.add(new View(createTable(viewName, enumTypeList, getViewsColumns(viewName))));
        }
        return views;
    }

    @Override
    public List<MaterializedView> getMaterializedViews(List<EnumType> enumTypeList) {
        String attribute = "materialized_view";
        List<MaterializedView> materializedViews = new ArrayList<>();

        List<String> materializedViewList = accessor
                .executeQuery("select matviewname as " + attribute
                        + " from pg_matviews where schemaname = '"
                        + accessor.configuration.getDatasource().getSchema()
                        + "'", attribute);

        for(String materializedViewName : materializedViewList) {
            materializedViews.add(new MaterializedView(createTable(materializedViewName, enumTypeList, getViewsColumns(materializedViewName))));
        }
        return materializedViews;
    }

    @Override
    public List<EnumType> getEnumTypes() {
        String query = "SELECT t.typname as enum_name, json_agg(e.enumlabel) as values from pg_type t" +
                " join pg_enum e on t.oid = e.enumtypid" +
                " join pg_catalog.pg_namespace n ON n.oid = t.typnamespace and n.nspname = '"
                + accessor.configuration.getDatasource().getSchema() + "'" +
                " group by t.typname";

        List<String> columnNames = new ArrayList<>();
        columnNames.add("enum_name");
        columnNames.add("values");
        List<Map<String, String>> results = accessor
                .executeQuery(query, columnNames);

        List<EnumType> enumTypeList = new ArrayList<>();
        for (Map<String, String> result : results) {
            enumTypeList.add(new EnumType(result));
        }
        return enumTypeList;
    }

    private List<Map<String, String>> getColumns(String tableName) {
        String queryPart1 = "SELECT" +
                " tc.constraint_name," +
                " kcu.column_name," +
                " ccu.table_name AS foreign_table_name," +
                " ccu.column_name AS foreign_column_name," +
                " pgi.indisunique as is_unique" +
                " FROM information_schema.table_constraints tc" +
                " JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name" +
                " JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_name = tc.constraint_name" +
                " JOIN pg_catalog.pg_class pgc ON pgc.relname = tc.table_name" +
                " JOIN pg_catalog.pg_attribute pga ON pgc.oid = pga.attrelid and pga.attname = kcu.column_name" +
                " LEFT JOIN pg_catalog.pg_index pgi on pgc.oid = pgi.indrelid and pga.attnum = ALL(pgi.indkey) and pgi.indisprimary = false" +
                " WHERE constraint_type = 'FOREIGN KEY' AND tc.table_name='" + tableName + "'";

        String queryPart2 = "SELECT" +
                " c.column_name," +
                " c.is_nullable," +
                " c.udt_name as type," +
                " c.character_maximum_length as length," +
                " CASE WHEN pgi.indisprimary=true THEN 'true' ELSE 'false' END as is_primary," +
                " CASE WHEN pgi.indisunique=true THEN 'true' WHEN fkeys.is_unique=true THEN 'true' ELSE 'false' END as is_unique," +
                " fkeys.constraint_name, fkeys.foreign_column_name, fkeys.foreign_table_name" +
                " FROM information_schema.columns c" +
                " JOIN pg_catalog.pg_class pgct ON pgct.relname = c.table_name" +
                " LEFT JOIN fkeys on fkeys.column_name = c.column_name" +
                " LEFT JOIN information_schema.constraint_column_usage ccu ON ccu.column_name = c.column_name AND ccu.table_name = c.table_name " +
                " and ccu.constraint_name in (select relname from pg_class where oid in (select indexrelid from pg_index where indrelid = pgct.oid))" +
                " LEFT JOIN pg_catalog.pg_class pgc on ccu.constraint_name = pgc.relname" +
                " LEFT JOIN pg_catalog.pg_index pgi on pgc.oid = pgi.indexrelid" +
                " WHERE c.table_schema = '" + accessor.configuration.getDatasource().getSchema() + "'" +
                " AND c.table_name   = '" + tableName + "'";

        String query = "with fkeys AS (" + queryPart1 + ") " + queryPart2;

        return accessor
                .executeQuery(query, setColumnNames());
    }

    private List<Map<String, String>> getViewsColumns(String tableName) {
        String queryPart1 = "SELECT" +
                " tc.constraint_name," +
                " kcu.column_name," +
                " ccu.table_name AS foreign_table_name," +
                " ccu.column_name AS foreign_column_name," +
                " pgi.indisunique as is_unique" +
                " FROM information_schema.table_constraints tc" +
                " JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name" +
                " JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_name = tc.constraint_name" +
                " JOIN pg_catalog.pg_class pgc ON pgc.relname = tc.table_name" +
                " JOIN pg_catalog.pg_attribute pga ON pgc.oid = pga.attrelid and pga.attname = kcu.column_name" +
                " LEFT JOIN pg_catalog.pg_index pgi on pgc.oid = pgi.indrelid and pga.attnum = ALL(pgi.indkey) and pgi.indisprimary = false" +
                " WHERE constraint_type = 'FOREIGN KEY' AND tc.table_name='" + tableName + "'";

        String queryPart2 = "SELECT" +
                " a.attname as column_name," +
                " CASE WHEN a.attnotnull = true THEN 'NO' ELSE 'YES' END as is_nullable," +
                " pgt.typname as type," +
                " CASE WHEN a.atttypmod = -1 THEN null WHEN a.atttypid IN (1042, 1043) THEN a.atttypmod - 4 WHEN a.atttypid IN (1560, 1562) THEN a.atttypmod END as length," +
                " CASE WHEN pgi.indisprimary = true THEN 'true' ELSE 'false' END as is_primary," +
                " CASE WHEN pgi.indisunique = true THEN 'true' WHEN fkeys.is_unique = true THEN 'true' ELSE 'false' END as is_unique," +
                " fkeys.constraint_name," +
                " fkeys.foreign_column_name," +
                " fkeys.foreign_table_name" +
                " FROM pg_attribute a" +
                " JOIN pg_class pgct on a.attrelid = pgct.oid" +
                " JOIN pg_namespace s on pgct.relnamespace = s.oid" +
                " JOIN pg_type pgt on a.atttypid = pgt.oid" +
                " LEFT JOIN fkeys on fkeys.column_name = a.attname" +
                " LEFT JOIN information_schema.constraint_column_usage ccu ON ccu.column_name = a.attname AND ccu.table_name = pgct.relname" +
                " and ccu.constraint_name in (select relname from pg_class where oid in (select indexrelid from pg_index where indrelid = pgct.oid))" +
                " LEFT JOIN pg_catalog.pg_class pgc on ccu.constraint_name = pgc.relname" +
                " LEFT JOIN pg_catalog.pg_index pgi on pgc.oid = pgi.indexrelid" +
                " WHERE a.attnum > 0 AND NOT a.attisdropped" +
                " AND s.nspname = '" + accessor.configuration.getDatasource().getSchema() + "'" +
                " AND pgct.relname = '" + tableName + "'" +
                " ORDER BY a.attnum";

        String query = "with fkeys AS (" + queryPart1 + ") " + queryPart2;

        return accessor
                .executeQuery(query, setColumnNames());
    }

    private List<Map<String, String>> getJoins(String tableName) {
        String queryPart1 = "SELECT" +
                " tc.constraint_name," +
                " ccu.column_name," +
                " tc.table_name AS foreign_table_name," +
                " kcu.column_name AS foreign_column_name," +
                " 'false' as is_primary," +
                " CASE WHEN pgi.indisunique=true THEN 'true' ELSE 'false' END as is_unique" +
                " FROM information_schema.table_constraints tc" +
                " JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name" +
                " JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_name = tc.constraint_name" +
                " JOIN pg_catalog.pg_class pgc ON pgc.relname = tc.table_name" +
                " JOIN pg_catalog.pg_attribute pga ON pgc.oid = pga.attrelid and pga.attname = kcu.column_name" +
                " LEFT JOIN pg_catalog.pg_index pgi on pgc.oid = pgi.indrelid and pga.attnum = ALL(pgi.indkey) and pgi.indisprimary = false" +
                " WHERE constraint_type = 'FOREIGN KEY' AND ccu.table_name='" + tableName + "'";

        String queryPart2 = "SELECT" +
                " c.column_name," +
                " c.is_nullable," +
                " c.udt_name as type," +
                " c.character_maximum_length as length," +
                " fkeys.is_primary," +
                " fkeys.is_unique," +
                " fkeys.constraint_name, " +
                " fkeys.foreign_column_name, " +
                " fkeys.foreign_table_name" +
                " FROM information_schema.columns c" +
                " INNER JOIN fkeys on fkeys.column_name = c.column_name" +
                " WHERE c.table_schema = '" + accessor.configuration.getDatasource().getSchema() + "'" +
                " AND c.table_name   = '" + tableName + "'";


        String query = "with fkeys AS (" + queryPart1 + ") " + queryPart2;

        return accessor
                .executeQuery(query, setColumnNames());
    }
}