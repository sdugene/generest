package com.sdugene.database;

import com.sdugene.enums.DbType;
import com.sdugene.models.Configuration;
import org.apache.maven.plugin.logging.Log;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Accessor {

    Configuration configuration;
    private Log log;

    private Connection connection;
    private Statement statement;

    public Accessor(Configuration configuration, Log log) {
        this.configuration = configuration;
        this.log = log;
    }

    public boolean createAccess() {

        Map<String, String> properties = configuration.getDatasource().getProperties();
        try {
            connection = DriverManager
                    .getConnection(properties.get("url"), properties.get("username"), properties.get("password"));

            statement = connection.createStatement();
        } catch (SQLException e) {
            log.info("SQl Error " + e.getMessage());
            return false;
        }
        return true;
    }

    List<String> executeQuery(String query, String columnName) {
        List<String> columnNames = new ArrayList<>();
        columnNames.add(columnName);

        List<Map<String, String>> results = executeQuery(query, columnNames);


        List<String> list = new ArrayList<>();
        for (Map<String, String> result : results) {
            list.add(result.get(columnName));
        }

        return list ;
    }

    List<Map<String, String>> executeQuery(String query, List<String> columnNames) {
        List<Map<String, String>> list = new ArrayList<>();
        try (ResultSet resultSet = statement.executeQuery(query)) {
            while(resultSet.next()) {
                Map<String, String> column = new HashMap<>();
                for (String columnName : columnNames) {
                    column.put(columnName, resultSet.getString(columnName));
                }
                list.add(column);
            }
        } catch (SQLException e) {
            return list;
        }

        return list;
    }

    public boolean closeAccess() {
        try {
            connection.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public SQLParser getSQLParser() {
        if (DbType.MYSQL.equals(configuration.getDatasource().getType())) {
            return new MySQLParser(this);
        } else if (DbType.POSTGRESQL.equals(configuration.getDatasource().getType())) {
            return new PostGreSQLParser(this);
        } else {
            return new GenericSQLParser();
        }
    }
}
