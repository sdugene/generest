package com.sdugene.models;

import com.sdugene.Constant;
import com.sdugene.enums.Type;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;

public class Column {

    private int index;
    private String name;
    private Integer length;
    private Type type;
    private boolean primary;
    private boolean unique;
    private String enumName;
    private boolean nullable;
    private String constraintName;
    private String fkTable;
    private String fkColumn;
    private String fkAttribute;

    public Column(Map<String, String> columnMap, List<EnumType> enumTypeList, String columnType, int index) {
        this.index = index;
        this.name = columnMap.get(Constant.COLUMN_NAME);
        this.length = setLength(columnMap.get("length"));
        this.type = Type.convert(columnMap.get("type"), enumTypeList);
        if (Type.ENUM.equals(this.type)) {
            this.enumName = columnMap.get("type");
        }
        this.primary = "true".equals(columnMap.get("is_primary"));
        this.unique = "true".equals(columnMap.get("is_unique"));
        this.nullable = "YES".equals(columnMap.get("is_nullable"));
        this.constraintName = columnMap.get("constraint_name");
        this.fkTable = columnMap.get("foreign_table_name");
        this.fkColumn = columnMap.get(Constant.FOREIGN_COLUMN_NAME);
        if (this.constraintName != null && "column".equals(columnType)) {
            if (columnMap.get(Constant.COLUMN_NAME).contains("_" + columnMap.get(Constant.FOREIGN_COLUMN_NAME))) {
                this.fkAttribute = columnMap.get(Constant.COLUMN_NAME)
                        .split("_" + columnMap.get(Constant.FOREIGN_COLUMN_NAME))[0];
            } else if (columnMap.get(Constant.COLUMN_NAME).contains(columnMap.get(Constant.FOREIGN_COLUMN_NAME) + "_")) {
                this.fkAttribute = columnMap.get(Constant.COLUMN_NAME)
                        .split(columnMap.get(Constant.FOREIGN_COLUMN_NAME) + "_")[1];
            } else {
                this.fkAttribute = columnMap.get(Constant.COLUMN_NAME);
            }
        }

        if (this.constraintName != null && "joinColumn".equals(columnType)) {
            if (columnMap.get(Constant.FOREIGN_COLUMN_NAME).contains("_" + columnMap.get(Constant.COLUMN_NAME))) {
                this.fkAttribute = columnMap.get(Constant.FOREIGN_COLUMN_NAME)
                        .split("_" + columnMap.get(Constant.COLUMN_NAME))[0];
            } else if (columnMap.get(Constant.FOREIGN_COLUMN_NAME).contains(columnMap.get(Constant.COLUMN_NAME) + "_")) {
                this.fkAttribute = columnMap.get(Constant.FOREIGN_COLUMN_NAME)
                        .split(columnMap.get(Constant.COLUMN_NAME) + "_")[1];
            } else {
                this.fkAttribute = columnMap.get(Constant.FOREIGN_COLUMN_NAME);
            }
        }
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return  StringUtils.lowerCase(name);
    }

    public String getSqlName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    private Integer setLength(String value) {

       if (value == null) {
            this.length = null;
        } else {
            this.length = Integer.valueOf(value);
        }

        return this.length;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isPrimary() {
        return primary;
    }

    public boolean isPrimary(List<Column> columnList) {
        return primary || columnList.stream().noneMatch(Column::isPrimary)
                && columnList.get(0).getName().equals(name);
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public boolean isUnique() {
        return unique;
    }

    public void setUnique(boolean unique) {
        this.unique = unique;
    }

    public String getEnumName() {
        return enumName;
    }

    public void setEnumName(String enumName) {
        this.enumName = enumName;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public String getConstraintName() {
        return constraintName;
    }

    public void setConstraintName(String constraintName) {
        this.constraintName = constraintName;
    }

    public String getFkTable() {
        return StringUtils.lowerCase(fkTable);
    }

    public String getSqlFkTable() {
        return fkTable;
    }

    public void setFkTable(String fkTable) {
        this.fkTable = fkTable;
    }

    public String getFkColumn() {
        return StringUtils.lowerCase(fkColumn);
    }

    public String getSqlFkColumn() {
        return fkColumn;
    }

    public void setFkColumn(String fkColumn) {
        this.fkColumn = fkColumn;
    }

    public String getFkAttribute() {


        return StringUtils.lowerCase(fkAttribute);
    }

    public String getSqlFkAttribute() {
        return fkAttribute;
    }

    public void setFkAttribute(String fkAttribute) {
        this.fkAttribute = fkAttribute;
    }

    @Override
    public String toString() {
        return "Column{" +
                "index='" + index + '\'' +
                ", name='" + name + '\'' +
                ", length=" + length +
                ", type=" + type +
                ", primary=" + primary +
                ", unique=" + unique +
                ", enumName='" + enumName + '\'' +
                ", nullable=" + nullable +
                ", constraintName='" + constraintName + '\'' +
                ", fkTable='" + fkTable + '\'' +
                ", fkColumn='" + fkColumn + '\'' +
                ", fkAttribute='" + fkAttribute + '\'' +
                '}';
    }
}
