package com.sdugene.models;

import com.sdugene.models.configurations.*;

public class Configuration {

    private Datasource datasource = new Datasource();
    private Java java = new Java();
    private Generest generest = new Generest();
    private Generated generated = new Generated();
    private Exclusions exclusions = new Exclusions();
    private Parameters parameters = new Parameters();
    private Documentations documentations = new Documentations();

    public Datasource getDatasource() {
        return datasource;
    }

    public void setDatasource(Datasource datasource) {
        this.datasource = datasource;
    }

    public Java getJava() {
        return java;
    }

    public void setJava(Java java) {
        this.java = java;
    }

    public Generest getGenerest() {
        return generest;
    }

    public void setGenerest(Generest generest) {
        this.generest = generest;
    }

    public Generated getGenerated() {
        return generated;
    }

    public void setGenerated(Generated generated) {
        this.generated = generated;
    }

    public Exclusions getExclusions() { return exclusions; }

    public void setExclusions(Exclusions exclusions) { this.exclusions = exclusions; }

    public Parameters getParameters() {
        return parameters;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    public Documentations getDocumentations() {
        return documentations;
    }

    public void setDocumentations(Documentations documentations) {
        this.documentations = documentations;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "datasource=" + datasource +
                ", java=" + java +
                ", generest=" + generest +
                ", generated=" + generated +
                ", exclusions=" + exclusions +
                ", parameters=" + parameters +
                ", documentations=" + documentations +
                '}';
    }
    }
