package com.sdugene.models;

public class MaterializedView extends View {

    public MaterializedView(Table table) {
        super(table);
        setColumnList(table.getColumnList());
        setJoinList(table.getJoinList());
        setView(true);
        setMaterializedView(true);
    }
}
