package com.sdugene.models.annotation;

import java.util.*;

public class Entity {
    private String name;
    private List<String> classImport;
    private List<String> classAnnotations;
    private Map<String, List<String>> attributeAnnotations;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getClassImport() {
        return classImport;
    }

    public void setClassImport(String classImport) {
        this.classImport =  Arrays.asList(classImport.trim().split(","));
    }

    public List<String> getClassAnnotations() {
        return classAnnotations;
    }

    public void setClassAnnotations(String classAnnotations) {
        this.classAnnotations = Arrays.asList(classAnnotations.trim().split(","));
    }

    public Map<String, List<String>> getAttributeAnnotations() {
        return attributeAnnotations;
    }

    public void setAttributeAnnotations(Map<String, String> attributeAnnotations) {
        this.attributeAnnotations = new HashMap<>();
        attributeAnnotations
                .forEach((k, v) -> this.attributeAnnotations.put(k, Arrays.asList(v.trim().split(","))));
    }

    @Override
    public String toString() {
        return "Entity{" +
                "name='" + name + '\'' +
                ", classAnnotation=" + classAnnotations +
                ", attributeAnnotations=" + attributeAnnotations +
                '}';
    }
}
