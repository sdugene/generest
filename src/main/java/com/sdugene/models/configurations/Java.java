package com.sdugene.models.configurations;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Java {

    private String folder;

    @JsonProperty("package")
    private String defaultPackage;

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getDefaultPackage() {
        return defaultPackage;
    }

    public void setDefaultPackage(String defaultPackage) {
        this.defaultPackage = defaultPackage;
    }

    public String getDefaultPackageDotted() {
        return defaultPackage != null ? defaultPackage + "." : null;
    }

    @Override
    public String toString() {
        return "Java{" +
                "folder='" + folder + '\'' +
                ", defaultPackage='" + defaultPackage + '\'' +
                '}';
    }
}