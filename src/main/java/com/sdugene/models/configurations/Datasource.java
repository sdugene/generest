package com.sdugene.models.configurations;

import com.sdugene.enums.DbType;

import java.util.HashMap;
import java.util.Map;

public class Datasource {

    private String url;
    private String username;
    private String password;
    private String schema;
    private DbType type;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
        setType(url);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public DbType getType() {
        return type;
    }

    private void setType(String url) {
        String[] typePart = url.split(":");
        type = DbType.getFromEnum(typePart[1]);

        if (type != null && !DbType.POSTGRESQL.equals(type)) {
            String[] basePart = url.split("/");
            String baseName = basePart[basePart.length-1];

            if (baseName.matches("[0-9a-z_-]+")) {
                schema = baseName;
            }
        }
    }

    public Map<String, String> getProperties() {
        Map<String, String> properties = new HashMap<>();

        properties.put("url", url);
        properties.put("username", username);
        properties.put("password", password);

        return properties;
    }

    @Override
    public String toString() {
        return "Datasource{" +
                "url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", schema='" + schema + '\'' +
                ", type='" + type.getTypeName() + '\'' +
                '}';
    }
}