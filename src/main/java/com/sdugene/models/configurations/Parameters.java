package com.sdugene.models.configurations;

import com.sdugene.enums.Type;

public class Parameters {

    private boolean lombok = false;
    private boolean lazyLoading = true;
    private boolean dtoChangeLog = true;
    private boolean jpaSpecification = true;
    private Type timestampType = Type.LOCAL_DATE_TIME;

    public boolean isLombok() {
        return lombok;
    }

    public void setLombok(boolean lombok) {
        this.lombok = lombok;
    }

    public boolean isLazyLoading() {
        return lazyLoading;
    }

    public void setLazyLoading(boolean lazyLoading) {
        this.lazyLoading = lazyLoading;
    }

    public boolean isDtoChangeLog() {
        return dtoChangeLog;
    }

    public void setDtoChangeLog(boolean dtoChangeLog) {
        this.dtoChangeLog = dtoChangeLog;
    }

    public boolean isJpaSpecification() {
        return jpaSpecification;
    }

    public void setJpaSpecification(boolean jpaSpecification) {
        this.jpaSpecification = jpaSpecification;
    }

    public Type getTimestampType() {
        return timestampType;
    }

    public void setTimestampType(Type timestampType) {
        this.timestampType = timestampType;
    }

    @Override
    public String toString() {
        return "Generated{" +
                "lombok=" + lombok +
                "lazyLoading=" + lazyLoading +
                "dtoChangeLog=" + dtoChangeLog +
                "jpaSpecification=" + jpaSpecification +
                "timestampType=" + timestampType +
                '}';
    }
}
