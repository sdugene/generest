package com.sdugene.models.configurations;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Documentations {

    private boolean swagger = true;

    @JsonProperty("api")
    private boolean apidoc = true;

    public boolean isSwagger() {
        return swagger;
    }

    public void setSwagger(boolean swagger) {
        this.swagger = swagger;
    }

    public boolean isApidoc() {
        return apidoc;
    }

    public void setApidoc(boolean apidoc) {
        this.apidoc = apidoc;
    }

    @Override
    public String toString() {
        return "Documentations{" +
                "swagger=" + swagger +
                ", apidoc=" + apidoc +
                '}';
    }
}