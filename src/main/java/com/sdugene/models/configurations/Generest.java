package com.sdugene.models.configurations;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Generest {

    @JsonProperty("package")
    private String packageName = "generest";
    private String suffix = "Gen";
    private String apiUri = "/api/v2";

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getApiUri() {
        return apiUri;
    }

    public void setApiUri(String apiUri) {
        this.apiUri = apiUri;
    }

    public String getPackageNameDoted() {
        return packageName != null ? packageName + "." : null;
    }

    @Override
    public String toString() {
        return "Generest{" +
                "packageName='" + packageName + '\'' +
                ", suffix='" + suffix + '\'' +
                ", apiUri='" + apiUri + '\'' +
                '}';
    }
}