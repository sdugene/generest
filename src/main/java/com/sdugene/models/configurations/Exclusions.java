package com.sdugene.models.configurations;

import java.util.*;

public class Exclusions {

    private final Set<String> entities = new HashSet<>();
    private final Set<String> repositories = new HashSet<>();
    private final Set<String> controllers = new HashSet<>();
    private final Set<String> dtos = new HashSet<>();
    private final Set<String> filters = new HashSet<>();
    private final Set<String> services = new HashSet<>();
    private final Set<String> mappers = new HashSet<>();

    public Exclusions() { }

    public Set<String> getEntities() {
        return entities;
    }

    public Set<String> getRepositories() {
        return repositories;
    }

    public Set<String> getControllers() {
        return controllers;
    }

    public Set<String> getDtos() {
        return this.dtos;
    }

    public Set<String> getFilters() {
        return filters;
    }

    public Set<String> getServices() {
        return services;
    }

    public Set<String> getMappers() {
        return mappers;
    }

    public void setEntities(HashMap<String, String> entityArray) {
        this.entities.addAll(entityArray.keySet());
        this.repositories.addAll(entityArray.keySet());
        this.controllers.addAll(entityArray.keySet());
        this.dtos.addAll(entityArray.keySet());
        this.filters.addAll(entityArray.keySet());
        this.services.addAll(entityArray.keySet());
        this.mappers.addAll(entityArray.keySet());
    }

    public void setRepositories(HashMap<String, String> repositoryArray) {
        this.repositories.addAll(repositoryArray.keySet());
        this.controllers.addAll(repositoryArray.keySet());
        this.dtos.addAll(repositoryArray.keySet());
        this.filters.addAll(repositoryArray.keySet());
        this.services.addAll(repositoryArray.keySet());
        this.mappers.addAll(repositoryArray.keySet());
    }

    public void setControllers(HashMap<String, String> controllerArray) {
        this.controllers.addAll(controllerArray.keySet());
    }

    public void setDtos(HashMap<String, String> dtoArray) {
        this.dtos.addAll(dtoArray.keySet());
        this.mappers.addAll(dtoArray.keySet());
        this.controllers.addAll(dtoArray.keySet());
    }

    public void setFilters(HashMap<String, String> filterArray) {
        this.filters.addAll(filterArray.keySet());
    }

    public void setServices(HashMap<String, String> serviceArray) {
        this.services.addAll(serviceArray.keySet());
        this.controllers.addAll(serviceArray.keySet());
    }

    public void setMappers(HashMap<String, String> mapperArray) {
        this.mappers.addAll(mapperArray.keySet());
        this.controllers.addAll(mapperArray.keySet());
    }

    @Override
    public String toString() {
        return "Exclusions{" +
                "entities=" + entities +
                ", repositories=" + repositories +
                ", controllers=" + controllers +
                ", dtos=" + dtos +
                ", filters=" + filters +
                ", services=" + services +
                ", mappers=" + mappers +
                '}';
    }
}
