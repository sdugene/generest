package com.sdugene.models.configurations;

public class Generated {

    private boolean controllers = true;
    private boolean dtos = true;
    private boolean filters = true;
    private boolean services = true;
    private boolean mappers = true;

    public boolean isControllers() {
        return controllers;
    }

    public void setControllers(boolean controllers) {
        this.controllers = controllers;
    }

    public boolean isDtos() {
        return dtos;
    }

    public void setDtos(boolean dtos) {
        this.dtos = dtos;
    }

    public boolean isFilters() {
        return filters;
    }

    public void setFilters(boolean filters) {
        this.filters = filters;
    }

    public boolean isServices() {
        return services;
    }

    public void setServices(boolean services) {
        this.services = services;
    }

    public boolean isMappers() {
        return mappers;
    }

    public void setMappers(boolean mappers) {
        this.mappers = mappers;
        this.dtos = mappers;
    }


    @Override
    public String toString() {
        return "Generated{" +
                "controllers=" + controllers +
                ", dtos=" + dtos +
                ", filters=" + filters +
                ", services=" + services +
                ", mappers=" + mappers +
                '}';
    }
}
