package com.sdugene.models;

import com.sdugene.models.annotation.Entity;

import java.util.ArrayList;
import java.util.List;

public class Annotation {

    private List<Entity> entities = new ArrayList<>();

    public Entity getEntities(String key) {
        return entities
                .stream()
                .filter(e -> e.getName().equals(key))
                .findFirst().orElse(null);
    }

    public void setEntities(List<Entity> entities) {
        this.entities = entities;
    }

    @Override
    public String toString() {
        return "Annotation{" +
                "entities=" + entities +
                '}';
    }
}
