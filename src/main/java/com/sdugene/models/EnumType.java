package com.sdugene.models;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EnumType {

    private String name;
    private List<String> valueList;

    @SuppressWarnings("unchecked")
    public EnumType(Map<String, String> enumMap) {
        this.name = enumMap.get("enum_name");
        try {
            this.valueList = new ObjectMapper().readValue(enumMap.get("values"), List.class);
        } catch (IOException e) {
            this.valueList = new ArrayList<>();
        }
    }

    public String getName() {
        return StringUtils.lowerCase(name);
    }

    public String getSqlName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getValueList() {
        return valueList;
    }

    public void setValueList(List<String> valueList) {
        this.valueList = valueList;
    }
}
