package com.sdugene.models;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class Table {

    private String name;
    private List<Column> columnList = new ArrayList<>();
    private List<Column> joinList = new ArrayList<>();
    private boolean isView = false;
    private boolean isMaterializedView = false;

    public Table(String name) {
        this.name = name;
    }

    public String getName() {
        return StringUtils.lowerCase(name);
    }

    public String getSqlName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Column> getColumnList() {
        return columnList;
    }

    public Column getPrimaryColumn() {
        for (Column column : columnList) {
            if (column.isPrimary()) return column;
        }
        return null;
    }

    public void setColumnList(List<Column> columnList) {
        this.columnList = columnList;
    }

    public List<Column> getJoinList() {
        return joinList;
    }

    public void setJoinList(List<Column> joinList) {
        this.joinList = joinList;
    }

    public boolean isView() {
        return isView;
    }

    public void setView(boolean view) {
        isView = view;
    }

    public boolean isMaterializedView() {
        return isMaterializedView;
    }

    public void setMaterializedView(boolean materializedView) {
        isMaterializedView = materializedView;
    }
}
