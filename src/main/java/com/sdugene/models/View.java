package com.sdugene.models;

public class View extends Table {

    public View(Table table) {
        super(table.getSqlName());
        setColumnList(table.getColumnList());
        setJoinList(table.getJoinList());
        setView(true);
    }
}
