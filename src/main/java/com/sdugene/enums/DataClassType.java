package com.sdugene.enums;

import com.sdugene.Constant;

public enum DataClassType {

    DTO(Constant.DTO, "", Constant.DTO, false, true),
    DTO_POST(Constant.DTO, "Post", Constant.DTO, false, false),
    DTO_FULL(Constant.DTO, "Full", Constant.DTO, true, true),
    ENTITY(Constant.ENTITY, "", Constant.ENTITY, true, true),
    ENTITY_BUILDER(Constant.ENTITY_BUILDER, "", Constant.ENTITY, true, false),
    FILTER(Constant.FILTER, "", Constant.FILTER, false, true);

    private final String type;
    private final String name;
    private final String suffix;
    private final boolean hasForeignKey;
    private final boolean hasPrimaryKey;

    DataClassType(String type, String name, String suffix, boolean hasForeignKey, boolean hasPrimaryKey) {
        this.type = type;
        this.name = name;
        this.suffix = suffix;
        this.hasForeignKey = hasForeignKey;
        this.hasPrimaryKey = hasPrimaryKey;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getComplete() { return name + suffix; }

    public boolean hasForeignKey() {
        return hasForeignKey;
    }

    public boolean hasPrimaryKey() {
        return hasPrimaryKey;
    }
}
