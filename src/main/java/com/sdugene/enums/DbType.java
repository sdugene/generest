package com.sdugene.enums;

import java.util.Arrays;
import java.util.List;

public enum DbType {

    POSTGRESQL("postgresql"),
    MYSQL("mysql");

    private final String typeName;

    DbType(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }

    public static DbType getFromEnum(String value) {
        List<String> accepted = Arrays.asList(POSTGRESQL.getTypeName(), MYSQL.getTypeName());

        if (accepted.contains(value.toLowerCase())) {
            for (DbType dbType : DbType.values()) {
                if (dbType.typeName.equalsIgnoreCase(value)) {
                    return dbType;
                }
            }
        }
        return null;
    }
}
