package com.sdugene.enums;

public enum Accessor {

    PUBLIC("public"),
    PROTECTED("protected"),
    PRIVATE("private"),
    PACKAGE_PRIVATE("");

    private final String access;

    Accessor(String access) {
        this.access = access;
    }

    public String getAccess() {
        return access;
    }
}
