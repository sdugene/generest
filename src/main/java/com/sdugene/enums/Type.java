package com.sdugene.enums;

import com.sdugene.models.EnumType;

import java.util.List;

public enum Type {

    STRING("String"),
    BIG_DECIMAL("BigDecimal"),
    BOOLEAN("Boolean"),
    INTEGER("Integer"),
    LONG("Long"),
    FLOAT("Float"),
    DOUBLE("Double"),
    LOCAL_DATE("LocalDate"),
    LOCAL_DATE_TIME("LocalDateTime"),
    LOCAL_TIME("LocalTime"),
    TIMESTAMP("Timestamp"),
    UUID("UUID"),
    ENUM("Enum"),
    JSONB("Jsonb"),

    OBJECT("Object");

    private final String typeName;

    Type(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }

    public static Type convert(String value, List<EnumType> enumTypeList) {
        switch (value) {
            case "character":
            case "varchar":
            case "text":
                return Type.STRING;
            case "numeric":
            case "decimal":
                return Type.BIG_DECIMAL;
            case "bit":
            case "bool":
                return Type.BOOLEAN;
            case "uuid":
                return Type.UUID;
            case "datetime":
                return Type.LOCAL_DATE_TIME;
            case "date":
                return Type.LOCAL_DATE;
            case "real":
                return Type.FLOAT;
            case "time":
                return Type.LOCAL_TIME;
            case "timestamp":
                return Type.TIMESTAMP;
            case "float":
                return Type.DOUBLE;
            case "bigint":
            case "int8":
                return Type.LONG;
            case "tinyint":
            case "smallint":
            case "integer":
            case "int4":
                return Type.INTEGER;
            case "jsonb":
                return Type.JSONB;
            default:
                return convertComplex(value, enumTypeList);
        }
    }

    private static Type convertComplex(String value, List<EnumType> enumTypeList) {
        for (EnumType enumType : enumTypeList) {
            if (enumType.getName().equals(value)) return Type.ENUM;
        }

        return Type.OBJECT;
    }
}
