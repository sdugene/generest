package com.sdugene;


import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.sdugene.models.Annotation;

import java.io.*;

import static com.sdugene.utils.StringUtils.inputStreamToString;

public class AnnotationReader {

    private Annotation annotation = new Annotation();

    boolean read(String annotationFile) throws IOException {
        File file = new File(annotationFile);

        if (!file.exists()) {
            return false;
        }

        XmlMapper xmlMapper = new XmlMapper();
        annotation = xmlMapper
                .readValue(inputStreamToString(new FileInputStream(file)), Annotation.class);
        return annotation != null;
    }

    public Annotation getAnnotation() { return annotation; }
}
