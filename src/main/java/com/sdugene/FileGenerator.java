package com.sdugene;

import com.sdugene.enums.DataClassType;
import com.sdugene.generator.*;
import com.sdugene.models.Annotation;
import com.sdugene.models.Configuration;
import com.sdugene.models.EnumType;
import com.sdugene.models.Table;
import com.sdugene.models.configurations.Exclusions;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static com.sdugene.enums.DataClassType.*;

class FileGenerator {

    private final Configuration configuration;
    private final Annotation annotation;

    List<String> errors = new ArrayList<>();
    private String generestFolder;

    FileGenerator(Configuration configuration, Annotation annotation) {
        this.configuration = configuration;
        this.annotation = annotation;
    }

    boolean createFolders(boolean isEnumListEmpty) {
        String path = new File(".").getPath() + File.separator + configuration.getJava().getFolder();
        generestFolder = path + File.separator + Function.packageToFolder(configuration.getGenerest().getPackageNameDoted());

        List<String> folders = new ArrayList<>();
        folders.add(generestFolder);
        folders.add(generestFolder + File.separator + Constant.ENTITIES);
        folders.add(generestFolder + File.separator + Constant.REPOSITORIES);
        folders.add(generestFolder + File.separator + Constant.REPOSITORIES + File.separator + Constant.RELATIONAL);

        if (configuration.getParameters().isJpaSpecification()) {
            folders.add(generestFolder + File.separator + Constant.REPOSITORIES + File.separator + Constant.SPECIFICATIONS);

        }

        if (configuration.getParameters().isDtoChangeLog() || !isEnumListEmpty) {
            folders.add(generestFolder + File.separator + Constant.ENUMS);
        }

        if (configuration.getGenerated().isFilters()) {
            folders.add(generestFolder + File.separator + Constant.ENTITIES + File.separator + Constant.FILTERS);
        }

        if (configuration.getGenerated().isControllers()) {
            folders.add(generestFolder + File.separator + Constant.CONTROLLERS);
        }

        if (configuration.getGenerated().isDtos()) {
            folders.add(generestFolder + File.separator + Constant.DTOS);
            folders.add(generestFolder + File.separator + Constant.DTOS + File.separator + "post");
            folders.add(generestFolder + File.separator + Constant.DTOS + File.separator + "full");
        }

        if (configuration.getGenerated().isMappers()) {
            folders.add(generestFolder + File.separator + Constant.MAPPERS);
        }

        if (configuration.getGenerated().isServices()) {
            folders.add(generestFolder + File.separator + Constant.SERVICES);
        }

        if (configuration.getDocumentations().isSwagger()) {
            folders.add(generestFolder + File.separator + Constant.CONFIGURATIONS);
        }

        return testFolder(folders);
    }

    private boolean testFolder(List<String> folders) {
        boolean result = true;
        for (String folder : folders) {
            if (!result) break;

            File f = new File(folder);
            if(!f.exists() || !f.isDirectory()) {
                result = new File(folder).mkdirs();
            }

            if(f.exists() && f.isDirectory()) {
                try {
                    FileUtils.cleanDirectory(folder);
                    result = true;
                } catch (IOException e) {
                    result = false;
                }
            }
        }
        return result;
    }

    boolean createFiles(List<Table> tables, List<EnumType> enumTypeList) {
        boolean result = true;

        if (configuration.getGenerated().isDtos()) {
            if (configuration.getParameters().isDtoChangeLog()) {
                result = createEntityInterfaceEnum() && createEntityEnum(tables);
            }
            result = result && createParentDto();
        }
        if (!enumTypeList.isEmpty()) {
            for (EnumType enumType : enumTypeList) {
                result = createEnumType(enumType);
            }
            result = result && createPostgreSQLEnumType();
        }

        for (Table table : tables) {
            if (!result) break;
            if (table.getColumnList() == null || table.getColumnList().isEmpty()) continue;
            result = createFile(table);
        }

        return result;
    }

    private boolean createFile(Table table) {
        String name = Function.snakeToCamel(table.getName());
        final Exclusions exclusions = configuration.getExclusions();
        boolean result = true;

        if (!exclusions.getEntities().contains(table.getName())) {
            result = createEntity(table, name);
        }

        if (result && !exclusions.getRepositories().contains(table.getName())) {
            result = createRepository(table, name);
            if (configuration.getParameters().isJpaSpecification()) {
                result = result && createRepositorySpecification(table, name);
            }
        }

        if (result && configuration.getGenerated().isFilters()
                && !exclusions.getFilters().contains(table.getName())) {
            result = createFilter(table, name);
        }

        if (result && configuration.getGenerated().isServices()
                && !exclusions.getServices().contains(table.getName())) {
            result = createService(table, name);
        }

        if (result && configuration.getGenerated().isDtos()
                && !exclusions.getDtos().contains(table.getName())) {
            result = createDto(table, name, DTO) && createDto(table, name, DTO_FULL);

            if (result && !table.isView()) {
                result = createDto(table, name, DTO_POST);
            }
        }

        if (result && configuration.getGenerated().isMappers()
                && !exclusions.getMappers().contains(table.getName())) {
            result = createMapper(table, name);
        }

        if (result && configuration.getGenerated().isControllers()
                && !exclusions.getControllers().contains(table.getName())) {
            result = createController(table, name);
        }

        if (result && configuration.getDocumentations().isSwagger()) {
            result = createSwaggerConfig();
        }
        return result;
    }

    private boolean createEntity(Table table, String name) {
        List<String> lines = EntityGenerator.generateClass(table, configuration, annotation);
        Path file = Paths.get(generestFolder + File.separator + Constant.ENTITIES + File.separator + name + Constant.ENTITY + configuration.getGenerest().getSuffix() + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean createFilter(Table table, String name) {
        List<String> lines = EntityGenerator.generateFilterClass(table, configuration);
        Path file = Paths.get(generestFolder + File.separator + Constant.ENTITIES + File.separator + Constant.FILTERS + File.separator + name + Constant.FILTER + configuration.getGenerest().getSuffix() + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean createRepository(Table table, String name) {
        List<String> lines = RepositoryGenerator.generateClass(table, configuration);
        Path file = Paths.get(generestFolder + File.separator + Constant.REPOSITORIES + File.separator + Constant.RELATIONAL + File.separator + name + Constant.REPOSITORY + configuration.getGenerest().getSuffix() + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean createRepositorySpecification(Table table, String name) {
        List<String> lines = RepositorySpecificationGenerator.generateClass(table, configuration);
        Path file = Paths.get(generestFolder + File.separator + Constant.REPOSITORIES + File.separator + Constant.SPECIFICATIONS + File.separator + name + Function.snakeToCamel(Constant.SPECIFICATIONS) + configuration.getGenerest().getSuffix() + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean createService(Table table, String name) {
        List<String> lines = ServiceGenerator.generateClass(table, configuration);
        Path file = Paths.get(generestFolder + File.separator + Constant.SERVICES + File.separator + name + Constant.SERVICE + configuration.getGenerest().getSuffix() + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean createParentDto() {
        String name = "Dto";
        List<String> lines = DtoGenerator.generateParentClass(configuration);
        Path file = Paths.get(generestFolder + File.separator + Constant.DTOS + File.separator + name + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean createDto(Table table, String name, DataClassType dataClassType) {
        if (table.isView() && DataClassType.DTO_POST.equals(dataClassType)) return true;
        List<String> lines = DtoGenerator.generateClass(table, configuration, dataClassType);
        String specificFolder = DTO.equals(dataClassType) ? "" : File.separator + dataClassType.getName().toLowerCase();

        Path file = Paths.get(generestFolder + File.separator + Constant.DTOS + specificFolder + File.separator + name + dataClassType.getName() + dataClassType.getSuffix() + configuration.getGenerest().getSuffix() + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean createMapper(Table table, String name) {
        List<String> lines = MapperGenerator.generateClass(table, configuration);
        Path file = Paths.get(generestFolder + File.separator + Constant.MAPPERS + File.separator + name + Constant.MAPPER + configuration.getGenerest().getSuffix() + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean createController(Table table, String name) {
        List<String> lines = ControllerGenerator.generateClass(table, configuration);
        Path file = Paths.get(generestFolder + File.separator + Constant.CONTROLLERS + File.separator + name + Constant.CONTROLLER + configuration.getGenerest().getSuffix() + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean createEntityEnum(List<Table> tables) {
        String name ="Attribute";
        List<String> lines = EnumGenerator.generateEnum(tables, configuration);
        Path file = Paths.get(generestFolder + File.separator + Constant.ENUMS + File.separator + name + configuration.getGenerest().getSuffix() + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean createEntityInterfaceEnum() {
        String name ="IAttribute";
        List<String> lines = EnumGenerator.generateAttributeInterface(configuration);
        Path file = Paths.get(generestFolder + File.separator + Constant.ENUMS + File.separator + name + configuration.getGenerest().getSuffix() + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean createEnumType(EnumType enumType) {
        String name = Function.snakeToCamel(enumType.getName());
        List<String> lines = EnumGenerator.generateEnum(enumType, configuration);
        Path file = Paths.get(generestFolder + File.separator + Constant.ENUMS + File.separator + name + configuration.getGenerest().getSuffix() + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean createPostgreSQLEnumType() {
        String name = "PostgreSQLEnumType";
        List<String> lines = EnumGenerator.generatePostgreSQLEnumType();
        Path file = Paths.get(generestFolder + File.separator + Constant.ENUMS + File.separator + name + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean createSwaggerConfig() {
        String name ="Swagger";
        List<String> lines = ConfigurationGenerator.generateSwaggerConfig(configuration);
        Path file = Paths.get(generestFolder + File.separator + Constant.CONFIGURATIONS + File.separator + name + Constant.CONFIG + configuration.getGenerest().getSuffix() + Constant.DOT_JAVA);
        return writeFile(file, lines);
    }

    private boolean writeFile(Path file, List<String> lines) {
        try {
            Files.write(file, lines, StandardCharsets.UTF_8);
        } catch (IOException e) {
            errors.add(e.getMessage());
        }

        File f = new File(file.toUri());
        return f.exists();
    }
}
