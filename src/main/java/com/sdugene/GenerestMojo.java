package com.sdugene;

import com.sdugene.database.Accessor;
import com.sdugene.database.SQLParser;
import com.sdugene.models.*;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

import java.io.IOException;
import java.util.List;

@Mojo( name = "run")
public class GenerestMojo extends AbstractMojo {

    private final ConfigurationReader configurationReader = new ConfigurationReader();

    public void execute() throws MojoExecutionException {
        try {
            String configurationFile = "generest.xml";
            getLog().info("Reading configuration from " + configurationFile);
            if (!configurationReader.read("./"+ configurationFile)) {
                throw new MojoExecutionException(listErrors(configurationReader.errors));
            }
        } catch (IOException e) {
            throw new MojoExecutionException(e.getMessage());
        }

        Accessor accessor = new Accessor(configurationReader.getConfiguration(), getLog());
        FileGenerator fileGenerator = new FileGenerator(
                configurationReader.getConfiguration(),
                getAnnotationConfiguration().getAnnotation()
        );

        if (!accessor.createAccess()) {
            throw new MojoExecutionException("database connexion failed");
        } else {
            getLog().info("Connexion success");
        }

        SQLParser sqlParser = accessor.getSQLParser();

        List<EnumType> enumTypeList = sqlParser.getEnumTypes();
        this.writeEnumLogs(enumTypeList);

        List<Table> tables = sqlParser.getTables(enumTypeList);
        this.writeTableLogs(tables);

        List<View> views = sqlParser.getViews(enumTypeList);
        this.writeViewLogs(views);

        List<MaterializedView> materializedViews = sqlParser.getMaterializedViews(enumTypeList);
        this.writeMaterializedViewLogs(materializedViews);

        if (!tables.isEmpty()) {
            if (!fileGenerator.createFolders(enumTypeList.isEmpty())) {
                throw new MojoExecutionException("generest folder creation failed");
            }

            tables.addAll(views);
            if (!fileGenerator.createFiles(tables, enumTypeList)) {
                throw new MojoExecutionException(listErrors(fileGenerator.errors));
            }

            tables.addAll(materializedViews);
            if (!fileGenerator.createFiles(tables, enumTypeList)) {
                throw new MojoExecutionException(listErrors(fileGenerator.errors));
            }
        }

        if (accessor.closeAccess()) {
            getLog().info("Everything went fine.");
        }
    }

    private String listErrors(List<String> errors) {
        for (String error : errors) {
            if (error != null) {
                return error;
            }
        }
        return "";
    }

    private void writeEnumLogs(List<EnumType> enumTypeList) {
        for (EnumType enumType : enumTypeList) {
            getLog().info(Function.logFind("enum",  enumType.getName(), enumType.getValueList().toString()));
        }
    }

    private void writeTableLogs(List<Table> tables) {
        for (Table table : tables) {
            getLog().info("find table named : " + table.getSqlName());

            for (Column column : table.getColumnList()) {
                getLog().info(Function.logFind("column", column.getSqlName(), column.toString()));
            }

            for (Column column : table.getJoinList()) {
                getLog().info(Function.logFind("joinded column", column.getSqlName(), column.toString()));
            }
        }
    }

    private void writeViewLogs(List<View> views) {
        for (View view : views) {
            getLog().info("find view named : " + view.getSqlName());
            for (Column column : view.getColumnList()) {
                getLog().info(Function.logFind("column", column.getSqlName(), column.toString()));
            }
        }
    }

    private void writeMaterializedViewLogs(List<MaterializedView> materializedViews) {
        for (MaterializedView materializedView : materializedViews) {
            getLog().info("find materialized view named : " + materializedView.getSqlName());
            for (Column column : materializedView.getColumnList()) {
                getLog().info(Function.logFind("column", column.getSqlName(), column.toString()));
            }
        }
    }

    private AnnotationReader getAnnotationConfiguration() {
        AnnotationReader annotationReader = new AnnotationReader();

        try {
            String annotationFile = "annotation_generest.xml";
            if (annotationReader.read("./"+ annotationFile)) {
                getLog().info("Reading configuration from " + annotationFile);
                return annotationReader;
            }
        } catch (IOException e) {
            return annotationReader;
        }
        return annotationReader;
    }



}
