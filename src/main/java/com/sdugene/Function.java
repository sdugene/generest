package com.sdugene;

import org.codehaus.plexus.util.StringUtils;

import java.io.File;
import java.util.regex.Matcher;

public class Function {

    private Function() {
        throw new IllegalStateException("Utility class");
    }

    public static String snakeToCamel(String text) {
        StringBuilder result = new StringBuilder();
        String[] words = text.split("_");

        for (String word : words) {
            result.append(StringUtils.capitalizeFirstLetter(word));
        }

        return result.toString();
    }

    public static String snakeToLowCamel(String text){
        return StringUtils.lowercaseFirstLetter(snakeToCamel(text));
    }

    static String packageToFolder(String value) {
        if (value != null) {

            value = value.replaceAll("\\.", Matcher.quoteReplacement(File.separator));
        }
        return value;
    }

    static String logFind(String founded, String name, String values) {
        return "find " + founded + " : " + name + " with " + values;
    }
}
