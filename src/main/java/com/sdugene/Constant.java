package com.sdugene;

public final class Constant {

    private Constant() {
        throw new IllegalStateException("Utility class");
    }

    public static final String ENTITIES = "entities";
    public static final String FILTERS = "filters";
    public static final String CONTROLLERS = "controllers";
    public static final String CONFIGURATIONS = "configurations";
    public static final String DTOS = "dtos";
    public static final String MAPPERS = "mappers";
    public static final String REPOSITORIES = "repositories";
    public static final String SPECIFICATIONS = "specifications";
    public static final String SERVICES = "services";
    public static final String ENUMS = "enums";
    public static final String RELATIONAL = "relational";

    public static final String ENTITY = "Entity";
    public static final String FILTER = "Filter";
    public static final String REPOSITORY = "Repository";
    public static final String SERVICE = "Service";
    public static final String DTO = "Dto";
    public static final String MAPPER = "Mapper";
    public static final String CONTROLLER = "Controller";
    public static final String CONFIG = "Config";
    public static final String BUILDER = "Builder";
    public static final String ENTITY_BUILDER = "EntityBuilder";

    public static final String COLUMN_NAME = "column_name";
    public static final String FOREIGN_COLUMN_NAME = "foreign_column_name";

    public static final String EMPTY_LINE = "";
    public static final String TAB_X_1 = "\t";
    public static final String TAB_X_2 = "\t\t";
    public static final String TAB_X_3 = "\t\t\t";

    public static final String NAME = "name";
    public static final String RETURN = "return";
    public static final String NOT_NULL = "!= null";
    public static final String TAB_AT_TRANSACTIONAL = TAB_X_1 + "@Transactional";
    public static final String TAB_AT_COLUMN = TAB_X_1 + "@Column";
    public static final String TAB_X_2_THIS = TAB_X_2 + "this";
    public static final String TAB_X_3_THIS = TAB_X_3 + "this";

    static final String DOT_JAVA = ".java";
    static final String UTF8 = "UTF-8";
}
